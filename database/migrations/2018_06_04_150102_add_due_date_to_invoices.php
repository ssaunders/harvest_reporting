<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDueDateToInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //



           Schema::table('invoices', function (Blueprint $table) {
            //
            $table->date('due_date')->after('issue_date')->nullable()->default(NULL); 
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
             Schema::table('invoices', function (Blueprint $table) {
            //
            $table->dropColumn("due_date");
           
        });
    }
}
