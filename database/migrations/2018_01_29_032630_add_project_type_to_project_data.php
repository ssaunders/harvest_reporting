<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProjectTypeToProjectData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('project_data', function (Blueprint $table) {
            //
            $table->string("project_type")->nullable()->after("project_name"); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('project_data', function (Blueprint $table) {
            //
            $table->dropColumn('project_type');
           
        });
    }
}
