<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_data', function (Blueprint $table) {
            //
            $table->integer("project_id"); 
            $table->string("project_name");
            $table->double("hourly_rate", 8, 2)->nullable();
            $table->double("project_budget", 10, 2)->nullable();
            $table->double("project_hours", 8, 2)->nullable();
            $table->double("effective_rate", 8, 2)->nullable();
            $table->dateTime("last_time_entry")->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('project_data');
    }
}
