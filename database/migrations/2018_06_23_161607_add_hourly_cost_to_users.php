<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHourlyCostToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            //
            
            $table->double("hourly_cost_rate", 10, 2)->after("non_billable_percentage")->default(0);
            $table->integer("non_working_days")->after("hourly_cost_rate")->default(28);
            $table->integer("billable_hours")->after("non_working_days")->default(0);
            $table->double("base_hourly_cost_rate", 10, 2)->after("non_billable_percentage")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            //
            $table->dropColumn("hourly_cost_rate");
            $table->dropColumn("non_working_days");
            $table->dropColumn("billable_hours");
            $table->dropColumn("base_hourly_cost_rate");
        });

    }
}
