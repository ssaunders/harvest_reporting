<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHarvestIdToInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::table('invoices', function (Blueprint $table) {
            //
            $table->bigInteger("harvest_id")->after("client_id")->nullable(); 


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('invoices', function (Blueprint $table) {
            //
            $table->dropColumn("harvest_id"); 


        });
    }
}
