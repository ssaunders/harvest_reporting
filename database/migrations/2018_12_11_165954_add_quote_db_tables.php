<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuoteDbTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('quotes', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('client_id')->nullable();
            $table->integer('staff_id');
            $table->integer('pipedrive_id')->nullable();
            $table->integer('harvest_project_id')->nullable();
            $table->text('description');            
            $table->timestamps();
        });

        Schema::create('quote_phases', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('quote_id');
            $table->text('phase_name');
            $table->integer('phase_order')->nullable();
            $table->text('start_date')->nullable();
            $table->text('end_date')->nullable();
            $table->timestamps();
        });

        Schema::create('quote_phase_lines', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('phase_id');
            $table->text('dept_name');
            $table->double('hours', 10, 2);
            $table->double('rate', 10,2);
            $table->double('staff_cost', 10,2)->nullable(); //<-- this assumes we know who is being assigned 
            $table->double('margin', 10,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('quotes');
        Schema::dropIfExists('quote_phases');
        Schema::dropIfExists('quote_phase_lines');


    }
}
