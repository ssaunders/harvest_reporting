<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProjectIdToInvoiceLines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::table('invoice_lines', function (Blueprint $table) {
            $table->bigInteger("project_id")->after('invoice_id')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    
         Schema::table('invoice_lines', function (Blueprint $table) {           
            $table->dropColumn("project_id"); 
        });
    
    }
}
