<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('harvest_expenses', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->bigInteger('harvest_expense_id');
            $table->bigInteger('project_id');
            $table->bigInteger('client_id');
            $table->bigInteger('staff_id');
            $table->text('expense_category')->nullable();
            $table->double('total_cost', 10, 2);
            $table->text('notes')->nullable();
            $table->text('status')->nullable();
            $table->boolean('is_billed')->default(false);
            
        });

        Schema::create('other_expenses', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->bigInteger('channel_id');
            $table->date('due_date');
            $table->date('date_paid');
            $table->enum('status', ['unpaid', 'paid'])->default('unpaid');
            $table->timestamps();
            $table->softDeletes();
            
        });

        Schema::create('expense_channels', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->text('channel');
            $table->timestamps();
            $table->softDeletes();
            
        });


         Schema::create('cash_balances', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->bigInteger('account');
            $table->date('date_updates');
            $table->double('balance', 10, 2);
            $table->timestamps();
            $table->softDeletes();
            
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('harvest_expenses');
        Schema::dropIfExists('other_expenses');
        Schema::dropIfExists('expense_channels');
        Schema::dropIfExists('cash_balances');
    }
}
