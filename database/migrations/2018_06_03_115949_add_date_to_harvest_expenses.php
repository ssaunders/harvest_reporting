<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDateToHarvestExpenses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::table('harvest_expenses', function (Blueprint $table) {
            //
            $table->date("spent_date")->after("status")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('harvest_expenses', function (Blueprint $table) {
            //
            $table->dropColumn("spent_date");
        });
    }
}
