<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('time_entries', function (Blueprint $table) {
            $table->bigInteger('id')->unique();
            $table->integer('project_id');
            $table->integer('employee_id');
            $table->text('employee_name');
            $table->double('entry', 10, 2);
            $table->date('date_entry');
            $table->boolean('billable');
            $table->double('bill_rate', 10, 2)->nullable();
            $table->text('notes')->nullable();
            $table->text('task_name')->nullable();
            $table->integer('task_id')->nullable();
           
            $table->index(['id','project_id', 'employee_id', 'task_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('time_entries');
    }
}
