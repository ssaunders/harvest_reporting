<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

         Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->bigInteger('client_id');
            $table->bigInteger('project_id')->nullable();
            $table->text('invoice_number');
            $table->text('status');
            $table->date('issue_date'); 
            $table->double('total_amount', 10, 2);
            $table->double("tax",10,2);
            
        });

         Schema::create("invoice_lines", function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->bigInteger('invoice_id'); 
            $table->text('kind');
            $table->double("amount",10,2);
          

         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
        Schema::dropIfExists('invoice_lines');
    }
}
