<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('users', function (Blueprint $table) {
            //
            $table->string("dept")->nullable()->after("remember_token"); 
            $table->double("salary",10,2)->after("dept"); 
            $table->double("agency_extras", 10, 2)->after("salary");
            $table->date("effective_date")->after("salary");


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

         Schema::table('users', function (Blueprint $table) {
            //
            $table->dropColumn('dept');
            $table->dropColumn('salary');
            $table->dropColumn('agency_extras');
            $table->dropColumn('agency_extras');
           
        });
    }
}
