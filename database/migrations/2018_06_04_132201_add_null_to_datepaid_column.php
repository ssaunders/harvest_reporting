<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullToDatepaidColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::table('other_expenses', function (Blueprint $table) {
            //
            $table->dropColumn("date_paid");
           
        });

           Schema::table('other_expenses', function (Blueprint $table) {
            //
            $table->date('date_paid')->after('due_date')->nullable()->default(NULL); 
           
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
