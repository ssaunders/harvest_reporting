<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClientsToProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        //

         Schema::table('project_data', function (Blueprint $table) {
            //
            $table->bigInteger("client_id")->after("project_name")->nullable(); 
            $table->text("client_name")->after("client_id")->nullable(); 


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('project_data', function (Blueprint $table) {
            //
            $table->dropColumn('client_id');
            $table->dropColumn('client_name');
           
        });
    }
}
