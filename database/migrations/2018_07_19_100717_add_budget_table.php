<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBudgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

         Schema::create('budget', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->text('name');
            $table->char('code', 10)->unique();
            $table->text('category')->nullable();
            $table->integer('budget_year')->default(2018);
            $table->double('allocation', 10, 2)->default(0);
            $table->text('notes')->nullable();

            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::dropIfExists('budget');
    }
}
