<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeeklyCostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::create('weekly_costs', function (Blueprint $table) {
            $table->bigInteger('id')->unique();
            $table->integer('reference_id'); 
            $table->text('cost_type');
            $table->double('cost', 10, 2);
            $table->integer('year_week');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::dropIfExists('weekly_costs');
    }
}
