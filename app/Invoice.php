<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Invoice extends Authenticatable
{
    use Notifiable;
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'harvest_id', 
        'project_id', 
        'invoice_number',
        'status', 
        'issue_date', 
        'due_date',
        'total_amount', 
        'tax'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function lines()
    {
        return $this->hasMany('App\InvoiceLine', 'invoice_id', 'id');
    }

    public function digitalMarketingLines()
    {
        return $this->hasMany('App\InvoiceLine', 'invoice_id', 'id')->digitalmarketinglines();
    }

    public function serviceFeeLines()
    {
        return $this->hasMany('App\InvoiceLine', 'invoice_id', 'id')->serviceFeeLines();
    }

    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id', 'client_id');
    }
    
     /**
     * Scope a query to only include sent invoices.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIssued($query)
    {
        return $query->where('status', '!=', 'draft');
    }


    

}
