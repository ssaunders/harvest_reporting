<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UtilizationReport extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $data;
    public $dept; 
    public $costs;
    public $human_date; 

    public function __construct($data, $dept, $costs, $date)
    {
        $this->data         = $data;
        $this->dept         = $dept;
        $this->costs        = $costs;
        $this->human_date   = $date;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

         if(env('APP_ENV') == "local")
        {
            setlocale(LC_MONETARY, 'en_US');
        }else
        {   
            setlocale(LC_MONETARY, 'en_US.utf8');
        }



        
    
        return $this->markdown('emails.utilizationreport')->with(['dept'=>$this->dept, "data"=>$this->data, "costs"=>$this->costs, "human_date"=>$this->human_date]);


    }
}
