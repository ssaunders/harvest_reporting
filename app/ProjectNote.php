<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectNote extends Model
{
    //
    protected $table = 'project_notes';
    protected $fillable = [
        'project_id', 
        'author_id', 
        'note_type',
		'author_id', 
		'content',];


    public function project()
    {
        return $this->belongsTo('App\Project', 'project_id', 'project_id');
    }

}
