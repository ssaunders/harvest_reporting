<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
	protected $table = 'project_data';
	public $timestamps = false;
    protected $primaryKey = 'project_id';
	
    //
    protected $fillable = [
        'project_id', 
        'project_name', 
        'hourly_rate', 
        'effective_rate', 
        'project_budget', 
        'project_hours', 
        'project_type', 
        'last_time_entry',
        'client_name', 
        'client_id',
        'is_active',
    ];

    protected $dates = ["last_time_entry"];
     

    public function clientInvoices()
    {
    	return $this->hasMany('App\Invoice','client_id', 'client_id');
    }

    
    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id', 'client_id');
    }


    public function expenses()
    {
        return $this->hasMany('App\HarvestExpense', 'project_id', 'project_id');
    }

    public function timesheets()
    {
        return $this->hasMany("App\TimeEntry", 'project_id', 'project_id');

    }

    public function notes()
    {
        return $this->hasMany("App\ProjectNote", "project_id", "project_id");
    }

}
