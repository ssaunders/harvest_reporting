<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeEntry extends Model
{
    //
    protected $table = 'time_entries';
	public $timestamps = false;
	protected $primaryKey = 'id';


	protected $fillable = [
        'project_id', 'id', 'employee_id', 'employee_name', 'entry', 'date_entry', 'billable', 'bill_rate', 'notes', 'task_name', 'task_id'
    ];
}
