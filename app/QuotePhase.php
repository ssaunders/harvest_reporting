<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class QuotePhase extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quote_id',
        'phase_name', 
        'phase_order', 
        'start_date',
        'end_date',
    ];


   
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function lines()
    {
        return $this->hasMany('App\QuotePhaseLine', 'phase_id', 'id');
    }

    
    public function quote()
    {
        return $this->belongsTo('App\Quote', 'quote_id', 'id');
    }


    // we can assume that the lines method above is including the contingency line 
    // note this works to exclude the line items 
    public function phaseCostsLow()
    {
        return $this->hasMany('App\QuotePhaseLine', 'phase_id', 'id')
                    ->costsLow()
                    ->selectRaw('phase_id, (SUM(hours) * 165) as cost')
                    ->groupBy('phase_id');

    }

    // only diff between these two are the lack of the scope modifier that removes the Contingency line
    public function phaseCostsHigh()
    {
        return $this->hasMany('App\QuotePhaseLine', 'phase_id', 'id')
                    ->selectRaw('phase_id, (SUM(hours) * 165) as cost')
                    ->groupBy('phase_id');

    }

    public function phaseHoursLow()
    {
        return $this->hasMany('App\QuotePhaseLine', 'phase_id', 'id')
                    ->costsLow()
                    ->selectRaw('phase_id, SUM(hours) as phase_hours')
                    ->groupBy('phase_id');

    }    

    public function phaseHoursHigh()
    {
        return $this->hasMany('App\QuotePhaseLine', 'phase_id', 'id')
                    ->selectRaw('phase_id, SUM(hours) as phase_hours')
                    ->groupBy('phase_id');

    }    
    

    

}
