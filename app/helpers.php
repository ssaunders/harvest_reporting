<?php 

use App\User;


    function makeAPICall($endpoint, $payload, $method='GET')
	{
		$config = config("services.harvest");
		$url = $config['harvest_url'].$endpoint;

		$headers = array();
		$headers[] = 'Harvest-Account-ID: '.$config['harvest_id'];
		$headers[] = 'Authorization: Bearer '.$config['harvest_token'];
		$headers[] = 'User-Agent:Reporting Application';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, 0);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_HEADER, false);
		
		// no payload on a GET request 
		if($payload)
		{
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($payload));			
		}

		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		
		

		// if we dont get a 200 or 201 then something has gone wrong and we can bail. 
		if($http_code != "201" &&  $http_code != "200") 
		{
			return FALSE; 
		}

		curl_close ($curl);
		return ($response); 

	}

    function pipedriveAPICall($stage_id, $endpoint="deals",$method='GET')
    {

            // Pipedrive API token
            $config = config("services.pipedrive");
            $api_token = $config['pipedrive_api_key'];
            $company_domain = $config['pipedrive_domain'];
             
            //URL for Deal listing with your $company_domain and $api_token variables
            $url = 'https://'.$company_domain.'.pipedrive.com/v1/'.$endpoint.'?stage_id='.$stage_id.'&status=open&api_token=' . $api_token;

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_VERBOSE, 0);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
            curl_setopt($curl, CURLOPT_HEADER, false);

            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($curl);
            
            // print("<pre>");
            // print_r($response);
            // print("</pre>");

            $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            
            

            // if we dont get a 200 or 201 then something has gone wrong and we can bail. 
            if($http_code != "201" &&  $http_code != "200") 
            {
                return FALSE; 
            }

            curl_close ($curl);
            return ($response); 
    }



	function getTheWeek($week_number=null,$nextyear=false, $year=2019)
    {

        $date = new \DateTime();
        $week = ($week_number == null) ? $date->format("W") : $week_number;
        //$year = $date->format("Y");
        if($nextyear) $year++;
        $date->setISODate($year,$week);
        return $date;

    }

    function getTheDatefromMonthDay($month, $day_of_month)
    {
        $date = \DateTime::createFromFormat('Y-n-j', '2018-'.$month.'-'.$day_of_month);
            
        return $date; 

    }

    function getStaffForWeek($week)
    {

        $date = getTheWeek($week, true);
        $week_start_date =  $date->format('Y-m-d');
        $end_of_week = date("Y-m-d", strtotime($week_start_date. ' + 6 days'));
        $month = $date->format('F');

        $active_staff_collection = User::where("active", 1)
                ->where('effective_date', "<=", $week_start_date)
                ->where("active", 1)
                ->where("dept", "!=", "")
                ->get();

         $filtered = $active_staff_collection->filter(function ($record) use ($week_start_date) {
                                        
                    if($record->non_billable_percentage == "100")
                    {
                        Log::debug('Removing '.$record->name." from the active staff collection");
                        return false;
                    }

                    // if they don't have an end date on file then they are still working
                    if(is_null($record->end_date))
                    {
                        return true;
                    }
                    

                    
                    // if the end date on file exceeds the start date of this week exclude them. 
                    if($record->end_date < $week_start_date)
                    {
                        return false;
                    }

                    
                   

                    // otherwise keep em
                    return true;
        });

        return $filtered;
         

    }

    function addToArrayByKeyWithValue($array, $key, $amount)
    {
        if(array_key_exists($key, $array))
        {
            $array[$key] += $amount; 
        }else
        {
            $array[$key] = $amount;  
        }

        return $array;
    }