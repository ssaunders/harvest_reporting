<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        
        $schedule->call(function () {
            dispatch(new \App\Jobs\GenerateTeamUtilizationReports());

        })->weekly()->tuesdays()->at('16:30')->name('utilization')->withoutOverlapping();



        $schedule->call(function () {
            dispatch(new \App\Jobs\GetInvoiceData());
            dispatch(new \App\Jobs\GetProjectData());
            dispatch(new \App\Jobs\GetClientData());
            dispatch(new \App\Jobs\GetExpenseData());
            Log::debug("Triggering Scheduler!");
        })->hourly();

        
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
