<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Quote extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'staff_id', 
        'pipedrive_id', 
        'harvest_project_id',
        'description',
    ];

   
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function phases()
    {
        return $this->hasMany('App\QuotePhase', 'quote_id', 'id');
    }

    

    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id', 'client_id');
    }

     public function staff()
    {
        return $this->belongsTo('App\User', 'staff_id', 'id');
    }
    
    

    

}
