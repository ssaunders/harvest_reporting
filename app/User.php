<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'harvest_id', 
       'name', 
       'email',
       'admin' ,
       'password',
       'dept', 
       'salary', 
       'agency_extras', 
       'effective_date', 
       'contactor', 
       'active', 
       'end_date', 
       'non_billable_percentage',
       'non_working_days',
       'hourly_cost_rate',
       'billable_hours',
       'base_hourly_cost_rate',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // protected $casts = [
    //     'admin' => 'boolean',
    //     'superadmin' => 'boolean'
    // ];



    public function isAdmin()
    {
        return $this->admin; // this looks for an admin column in your users table
    }

    public function isSuperAdmin()
    {
        return $this->superadmin;
    }
}


