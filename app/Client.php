<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Client extends Authenticatable
{
    use Notifiable;
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'client_name', 
        'is_active', 
        'currency',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function invoices()
    {
        return $this->hasMany('App\Invoice', 'client_id', 'client_id');
    }

    public function issuedInvoices()
    {
        return $this->hasMany('App\Invoice', 'client_id', 'client_id')->issued();
    }


    public function projects()
    {
        return $this->hasMany('App\Project', 'client_id', 'client_id');
    }

    public function expenses()
    {
        return $this->hasMany('App\HarvestExpense', 'client_id', 'client_id');
    }

    /*  
        Just a little note for later that we may want to look at a 
        HasManyThrough model definition here to make accessing invoice line items easier 
    */
    
}
