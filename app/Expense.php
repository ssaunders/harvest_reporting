<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Expense extends Authenticatable
{
    use Notifiable;

    protected $table = "other_expenses";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'channel_id',
        'amount',
        'due_date', 
        'date_paid', 
        'status'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function channel()
    {
        return $this->belongsTo('App\ExpenseChannel', 'channel_id', 'id');
    }

    

       
     /**
     * Scope a query to only include stuff that isn't paid yet.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePending($query)
    {
        return $query->where('status', 'unpaid');
    }


    

}
