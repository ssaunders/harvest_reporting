<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ProjectNote extends Form
{
    public function buildForm()
    {   
        $data = $this->getData();

       


        
        $this
            ->add('note_type', 'select', [
    								'choices' => [	'general' => 'General Note', 
    												'budget' => 'Budget Note',
    												'client_aware'=>'Client Note',
    												'internal'=>'Internal Note',
    												]])
            ->add('content', 'textarea')
            ->add('submit', 'submit', ['label' => 'Save Note']);
    }
}
