<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class CashBalance extends Form
{
    public function buildForm()
    {
        $this
            ->add('account', 'select', [
    								'choices' => [	'RBC CAD Chequing' => 'RBC CAD Chequing', 
    												'RBC USD Savings' => 'RBC USD Savings',
    												'Tangerine'=>'Tangerine',
    												'WayPay'=>'WayPay',
    												'PayPal'=>'PayPal'
    												],
    												'empty_value' => '=== Select Account ==='])
            ->add('date_updates', 'date', ["label"=>"Date", 'default_value'=>date('Y-m-d')])
            ->add('balance', 'number');
    }
}
