<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\ExpenseChannel;


class OtherExpense extends Form
{
    public function buildForm()
    {
       	// get the Expense Channels
       	$channels = ExpenseChannel::all();
       	$select_channels = $channels->pluck('channel', 'id');


        $this
            ->add('channel_id', 'select', [
    								'choices' => $select_channels->toArray(),
    												'empty_value' => '=== Select Expense Channel ===',
    												'label'=>'Account Channel'])
            ->add('due_date', 'date', ['default_value'=> date('Y-m-d', strtotime("+15 days"))])
            ->add('date_paid', 'date', ['default_value'=>''])
            ->add('status', 'select', ['choices'=>['unpaid'=>"unpaid", 'paid'=>"paid"], 'default_value'=>'unpaid'])
            ->add('amount', 'text')
            ->add('id', 'hidden');

            // TODO: Extend this functionality to allow for an <input type="number" step=".01" to provide native type checking for floats (money)
            
    }
}
