<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ExpenseChannel extends Form
{
    public function buildForm()
    {
        $this
            ->add('channel', 'text');
    }
}
