<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class QuotePhaseLine extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phase_id',
        'dept_name', 
        'hours', 
        'rate',
        'staff_cost',
        'margin',
        'note',
    ];



   
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function phase()
    {
        return $this->belongsTo('App\QuotePhase', 'phase_id', 'id');
    }

    public function scopeCostsLow($query)
    {
        return $query->where('dept_name', '!=', 'Contingency');
    }



    
    

    

}
