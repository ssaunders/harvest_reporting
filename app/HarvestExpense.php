<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class HarvestExpense extends Authenticatable
{
    use Notifiable;

    protected $table = "harvest_expenses";
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'harvest_expense_id',
        'project_id',
        'client_id',
        'staff_id',
        'expense_category',
        'total_cost',
        'notes',
        'status',
        'spent_date',
        'is_billed'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function project()
    {
        return $this->hasOne('App\Project', 'project_id', 'project_id');
    }

    public function staff()
    {
        return $this->belongsTo('App\User', 'staff_id', 'harvest_id');
    }

    public function client()
    {
        return $this->belongsTo('App\Client','client_id','client_id');

    }

    

}
