<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class InvoiceLine extends Authenticatable
{
    use Notifiable;
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'invoice_id', 
        'kind', 
        'amount',
        'project_id',
      
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function invoice()
    {
        return $this->belongsTo('App\Invoice', 'id', 'invoice_id');
    }

    public function  scopeDigitalMarketingLines($query)
    {
        return $query->where('kind', 'Digital Marketing Services');
    }


    // basically everything we get paid for
    public function scopeServiceFeeLines($query)
    {
        return $query->where('kind', "!=",'Media')->where('kind', "!=",'Expenses');
    }
}
