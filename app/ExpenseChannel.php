<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ExpenseChannel extends Authenticatable
{
    use Notifiable;

    protected $table = "expense_channels";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'channel'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function expenses()
    {
        return $this->hasMany('App\Expense', 'id', 'channel_id');
    }





    

}
