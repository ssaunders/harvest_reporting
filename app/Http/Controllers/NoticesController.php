<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\User;
use App\ApplicationNotice;
use Config;
use Log; 




class NoticesController extends Controller
{
   
    public function showNotices()
    {
        $notices = ApplicationNotice::all()->orderby('created_at', 'DESC');
        return view("auth.application_notices", ["notices"=>$notices]);

    }   

    

}
