<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\TimeEntry;
use App\Project;
use Log;

use App\Mail\UtilizationReport; 



class ReportController extends Controller
{
    //

    public function sendTest()
    {
    	 Mail::to("spence@artscience.ca")->send(new UtilizationReport(null));
    }

    public static function sendWeeklyUtilizationReport()
    {
    	Log::debug("Generating Weekly Utilization Report");
        
        // what is the week of the year right now ?
        $week_of_the_year  = (int) date('W');
        // now get the previous week
        $last_week = $week_of_the_year-1;
        if($last_week <= 0) $last_week = 52; // for the first week in Jan 
        
        $agency_fixed_costs = 800000;

        // get an index of projects and their bill rates 
        $projects = Project::all();
        $project_rates = $projects->pluck('effective_rate', 'project_id');

        
        $filtered = getStaffForWeek($last_week);// method in helpers.php

        $active_staff = count($filtered);
              
       
        // factor non-billables into the agency fixed costs 
        $non_billables = User::whereNotNull("non_billable_percentage")->get();
        foreach ($non_billables as $non_billable_user) {
              $salary = $non_billable_user->salary + $non_billable_user->agency_extras; 
              $attribute_to_agency = ($salary) * ($non_billable_user->non_billable_percentage/100);
              $agency_fixed_costs += $attribute_to_agency;
        }


        $individual_share       = ceil(($agency_fixed_costs/52)/$active_staff);  // for the week
        

        $dept_costs = [];

        foreach ($filtered as $staff) 
        {
           

            $staff_cost = (($staff->salary + $staff->agency_extras) / 52) + $individual_share; // for the week
            //$costs_by_employee[$staff->dept][$staff->name] = $staff_cost; 

            if(!array_key_exists($staff->dept, $dept_costs))
            {
                 $o = new \stdClass();
                 $o->dept = $staff->dept;
                 $o->dept_cost = $staff_cost;
                 $o->extras = $staff->agency_extras;
                 $o->team_count = 1; 
                 $dept_costs[$staff->dept] = $o; 
                 continue;
            }

            $o = $dept_costs[$staff->dept];

            $o->dept_cost += $staff_cost;
            $o->extras += $staff->agency_extras;
            $o->team_count += 1; 
            $dept_costs[$staff->dept] = $o; 
        }		

        // get the revenue by timesheets 
        $revenue = self::revenueByWeekNumber($last_week,$project_rates); //<-- this is REPEATED in this class from HarvestController... need to abstract it. 
        $revenue_for_week = $revenue->revenue_by_dept;
        $date = getTheWeek($last_week);
        $this_week = $date->format('Y-m-d'); 
        $end_of_week = date("Y-m-d", strtotime($this_week. ' + 6 days'));
        $human_date = $this_week." to ".$end_of_week ;


       	// hackery for now... 
        //Mail::to("spence@artscience.ca")->send(new UtilizationReport($revenue, "Marketing", $dept_costs));
       	 Mail::to("adam@artscience.ca")->send(new UtilizationReport($revenue, "Marketing", $dept_costs, $human_date));
       	 Mail::to("kirk@artscience.ca")->send(new UtilizationReport($revenue, "Creative", $dept_costs, $human_date));
       	 Mail::to("tom@artscience.ca")->send(new UtilizationReport($revenue, "Development", $dept_costs, $human_date));
       	 Mail::to("margaret@artscience.ca")->send(new UtilizationReport($revenue, "PM", $dept_costs, $human_date));

         // copies for spence
         Mail::to("spence@artscience.ca")->send(new UtilizationReport($revenue, "Marketing", $dept_costs, $human_date));
         Mail::to("spence@artscience.ca")->send(new UtilizationReport($revenue, "Creative", $dept_costs, $human_date));
         Mail::to("spence@artscience.ca")->send(new UtilizationReport($revenue, "Development", $dept_costs, $human_date));
         Mail::to("spence@artscience.ca")->send(new UtilizationReport($revenue, "PM", $dept_costs, $human_date));
      
    }


     public static function revenueByWeekNumber($week_number=1, $project_rates)
    {
        if(env('APP_ENV') == "local")
        {
            setlocale(LC_MONETARY, 'en_US');
        }else
        {   
            setlocale(LC_MONETARY, 'en_US.utf8');
        }

        $date = getTheWeek($week_number);
        $this_week = $date->format('Y-m-d'); 
        $end_of_week = date("Y-m-d", strtotime($this_week. ' + 6 days'));

        Log::debug($this_week ." - ".$end_of_week);

        $week_of_the_year  = (int) date('W'); // this is THIS week

        $entries = TimeEntry::whereBetween('date_entry', [$this_week, $end_of_week])->get();

        // staff and dept association 
        $staff_by_dept = [];
        $staff = User::where("active", 1)->get();


        foreach($staff as $employee)
        {
            
            if($employee->dept == "") continue;

            $staff_by_dept[$employee->harvest_id] = $employee->dept; 

        }
   

        $total_earned_revenue = 0; 

        $revenue_by_employee = [];
        $revenue_by_dept    = [];
        $hours_earned_by_dept = [];
        $hours_by_dept_by_employee = [];

        foreach ($entries as $entry) 
        {
            if($entry->billable) 
            {
                $amount = $entry->entry * $project_rates[$entry->project_id];
                // Log::debug("-- Finding ".$entry->entry." for ". $entry->employee_id ." at ".$entry->date_entry. " bla blah of time at a rate of ".$project_rates[$entry->project_id]. "for a total of ".money_format('%1n', $amount));

                $total_earned_revenue += $amount;


                $hours_earned_by_dept = addToArrayByKeyWithValue($hours_earned_by_dept, $staff_by_dept[$entry->employee_id], $entry->entry );

                // gotta check 2 keys now...

                if(array_key_exists($staff_by_dept[$entry->employee_id], $revenue_by_employee))
                {
                    // we have a dept 
                    if(array_key_exists($entry->employee_name, $revenue_by_employee[$staff_by_dept[$entry->employee_id]]))
                    {
                        $revenue_by_employee[$staff_by_dept[$entry->employee_id]][$entry->employee_name] += $amount; 
                        $hours_by_dept_by_employee[$staff_by_dept[$entry->employee_id]][$entry->employee_name] += $entry->entry;
                    }else
                    {
                        $revenue_by_employee[$staff_by_dept[$entry->employee_id]][$entry->employee_name] = $amount; 
                        $hours_by_dept_by_employee[$staff_by_dept[$entry->employee_id]][$entry->employee_name] = $entry->entry;
                    }
                }else // no dept key yet
                {
                    $revenue_by_employee[$staff_by_dept[$entry->employee_id]] = [];
                    $hours_by_dept_by_employee[$staff_by_dept[$entry->employee_id]]= [];

                    $revenue_by_employee[$staff_by_dept[$entry->employee_id]][$entry->employee_name] = $amount; 
                    $hours_by_dept_by_employee[$staff_by_dept[$entry->employee_id]][$entry->employee_name] = $entry->entry;
                }

                

                if(array_key_exists($staff_by_dept[$entry->employee_id], $revenue_by_dept))
                {
                    $revenue_by_dept[$staff_by_dept[$entry->employee_id]] += $amount;
                }else
                {
                    $revenue_by_dept[$staff_by_dept[$entry->employee_id]] = $amount;
                }
            }
        }

        $o = new \stdClass();
        $o->revenue_by_dept = $revenue_by_dept;
        $o->revenue_by_employee = $revenue_by_employee;
        $o->hours_earned_by_dept = $hours_earned_by_dept;
        $o->hours_by_dept_by_employee = $hours_by_dept_by_employee;

        
        return $o;
        

    }
    // this should probably move to helpers.php 
    
}
