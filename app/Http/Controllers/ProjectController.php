<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\User;
use App\Client;
use App\Project;
use App\ProjectNote;
use App\HarvestExpense; 
use Auth; 
use DB; 
use Log; 

use Kris\LaravelFormBuilder\FormBuilder;

class ProjectController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('admin');
    }


    public function addNoteView(FormBuilder $formBuilder, $project_id)
    {

       
        $form = $formBuilder->create(\App\Forms\ProjectNote::class, 
            [
                'method' => 'POST',
                'url' => route('postAddNote', ['project_id'=>$project_id])
            ],
            [
               
                'project_id'=>$project_id
            ]
        );


        return view('auth.project.addnote', compact('form'));
    }

    public function postAddNote(FormBuilder $formBuilder, $project_id)
    {
        $form = $formBuilder->create(\App\Forms\ProjectNote::class);

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $fields = $form->getFieldValues();
        $fields['author_id'] =  Auth::user()->id; 
        $fields['project_id'] =  $project_id;
            

        ProjectNote::create($fields);

        // go back to financial outlook 
        return redirect()->route('projectdetails', ['project_id'=>$project_id]); 
    }

    public function showProjectDetails($project_id)
    {

        if(env('APP_ENV') == "local")
        {
            setlocale(LC_MONETARY, 'en_US');
        }else
        {   
            setlocale(LC_MONETARY, 'en_US.utf8');
        }

        // get the project and its contingent parts
        $project = Project::where('project_id', $project_id)
                                ->with('clientInvoices', 'clientInvoices.lines')
                                ->with('expenses')
                                ->with('timesheets')
                                ->with('notes')
                                ->first();
        // set up some dummy data to keep things a little clean in the view
        $invoiced = [];
        $invoiced['Services'] = 0;
        $invoiced['Digital Marketing Services'] = 0;
        $invoiced['Expenses'] = 0;
        $invoiced['Media'] = 0;

        foreach($project->clientInvoices as $invoice)
        {
            if($invoice->status == "draft") continue;

            foreach ($invoice->lines as $line) 
            {
               if($project->project_id == $line->project_id)
               {
                    $key = $line->kind;
                     
                    $invoiced[$key] += $line->amount;
                     
               }
            }

        }

        // summarize the timesheets by month 
        $time_months_billable = [];
        $time_months_nonbillable = [];
        foreach ($project->timesheets as $entry) {
            // get the date of the entry 
            $day    = new \DateTime($entry->date_entry);
            $month  = $day->format("M");
            $year   = $day->format("Y");
            if(!array_key_exists($month." ".$year, $time_months_billable))
            {
                $time_months_billable[$month." ".$year] = 0; 
                $time_months_nonbillable[$month." ".$year] = 0; 
            }

            if($entry->billable == 1)
            {
                 $time_months_billable[$month." ".$year] += $entry->entry;
            }else
            {
                $time_months_nonbillable[$month." ".$year] += $entry->entry;
            }
           
        }



        return view("auth.project.details", ["project"=>$project, 
                                                "invoiced"=>$invoiced,
                                                "time_months_billable"=>$time_months_billable,
                                                "time_months_nonbillable"=>$time_months_nonbillable]);




    }

    
}
