<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Project;
use App\TimeEntry;
use App\User;
use App\Invoice;
use App\InvoiceLine;
use App\Employee;
use App\PLBatch; 
use App\TimeEntryAttributed; 
use App\HarvestExpense; 
use App\HarvestExpenseAttributed; 
use Config;
use Log; 




class InvoiceController extends Controller
{
    //

    /* so this method should be used / expanded on to provide a downloadable report for our bookeeper*/

    public function getInvoicesByDate($from, $to, $status=null)
    {
        $invoices = makeAPICall('invoices?from='.$from."&to=".$to, null);

        if($invoices === FALSE)  return "API Issue";
        
        $invoice_index = [];

        $all = json_decode($invoices);
       
        //print("<pre>"); 
        $o = ("Invoice Date\tInvoice Number\tClient\tInvoice Amount\tTax\tInvoice Total");

        if($status == "overdue")
        {
            $o = ("Invoice Date\tDue Date\tDays Late\tInvoice Number\tClient\tInvoice Amount\tTax\tInvoice Total");
        }

        // hackety hack 
        for($i=1; $i<=$all->total_pages; $i++)
        {
            $invoices = makeAPICall("invoices?from=".$from."&to=".$to."&page=".$i, null);
            $all_invoices = json_decode($invoices);

            $o .= self::printInvoices($all_invoices, $status);

            
        }

        return response($o)->header("Content-Type", "text/plain");

        //print("</pre>");

    }


   
   

    private static function printInvoices($all, $status)
    {

        //print("\n---");
        $o = "";
        foreach($all->invoices as $invoice )
        {
            // this little bastard is an ugly quick hack to get an aged receivables report out to the bank. 
            if(!is_null($status))
            {
                if($status == "overdue")
                {

                    if($invoice->state == "open")
                    {

                        // caculate days overdue
                        $now = time(); // or your date as well
                        $due_date = strtotime($invoice->due_date);
                        $datediff = $now - $due_date;

                        $days_late  = round($datediff / (60 * 60 * 24));

                        if($days_late < 0) $days_late = 0; 

                        $o .= ("\n".$invoice->issue_date);
                        $o .= ("\t".$invoice->due_date);
                        $o .= ("\t".$days_late);
                        $o .= ("\t=\"".$invoice->number."\"");
                        $o .= ("\t".$invoice->client->name);
                        $o .= ("\t".($invoice->amount - $invoice->tax_amount));
                        $o .= ("\t".$invoice->tax_amount);
                        $o .= ("\t".$invoice->amount);
                    }
                }
            }else
            {

                $invoice_date   = $invoice->issue_date;
                $month          = date('F',strtotime($invoice_date));

                $o .= ("\n".$invoice->issue_date);
                $o .= ("\t=\"".$invoice->number."\"");
                $o .= ("\t".$invoice->client->name);
                $o .= ("\t".($invoice->amount - $invoice->tax_amount));
                $o .= ("\t".$invoice->tax_amount);
                $o .= ("\t".$invoice->amount);
            }

        }

        return $o;

    }

    public static function getInvoices()
    {
        DB::table('invoices')->truncate();
        DB::table('invoice_lines')->truncate();
        $invoices = makeAPICall('invoices', null); 
        
        if($invoices === FALSE)
        
        {
            return "API Issue";
        }

        $all = json_decode($invoices);

        for($i=1; $i<=$all->total_pages; $i++)
        {
            $invoices = makeAPICall("invoices?page=".$i, null);
            $all_invoices = json_decode($invoices);
            self::saveInvoices($all_invoices);
        }

        Log::debug("INVOICE DATA IMPORTED (".$all->total_pages." pages)");


    }

    public function getInvoiceAmounts()
    {
        if(env('APP_ENV') == "local")
        {
            setlocale(LC_MONETARY, 'en_US');
        }else
        {   
            setlocale(LC_MONETARY, 'en_US.utf8');
        }

        $invoices = Invoice::orderby("issue_date", 'ASC')->with("lines")->get();
        
        $total_invoices=0; 
        $invoice_index = [];
        $months = [];
        
        self::sortInvoiceData($months, $total_invoices, $invoice_index, $invoices);

        return view("auth.invoice_summary", ["months"=>$months, "total_invoices"=>$total_invoices]);

    }

    // takes the response from the Harvest API and parses it into the data model 
    private static function saveInvoices($invoices)
    {
        // make an invoice
        foreach($invoices->invoices as $invoice)
        {
            Log::debug("Harvest invoice ".$invoice->id);

            $new_invoice = new Invoice(["client_id"=>$invoice->client->id,
                "harvest_id"=>$invoice->id,
                "project_id"=>NULL,
                "invoice_number"=>$invoice->number,
                "status"=>$invoice->state, 
                "issue_date"=>$invoice->issue_date,
                'due_date'=>$invoice->due_date,
                "total_amount"=>$invoice->amount,
                "tax"=>$invoice->tax_amount]);
            
            $new_invoice->save();
            $invoice_id = $new_invoice->id;

            Log::debug("invoice ".$invoice_id." saved!");
            $linecount = 0; 

            // now save the lines
            foreach ($invoice->line_items as $line) {

                // check if there is a project associated 
                $project_id = NULL; 

                if($line->project)
                {
                    $project_id = $line->project->id;
                }

                $new_line = new InvoiceLine(["invoice_id"=>$invoice_id,
                    "kind"=>$line->kind,
                    "amount"=>$line->amount,
                    "project_id"=>$project_id]);

                $new_line->save();
                $linecount++;

            }

            Log::debug("invoice has ".$linecount." lines.");

        }

    }


    private static function sortInvoiceData(&$months, &$totalcount, &$invoice_index, $all)
    {
        // use this as an estimate for now 
        $yearly_target = 4000000;  
        $monthly_target = round($yearly_target / 12, 2); 
        $rolling_total = 0; 
        $rolling_target = 0; 

        foreach($all as $invoice )
        {

            //if(array_key_exists($invoice->number, $invoice_index)) continue;

            $invoice_index[$invoice->invoice_number] = 0; 

            $totalcount++;

            $invoice_date   = $invoice->issue_date;
            $month          = date('F',strtotime($invoice_date));
            $year           = date('Y', strtotime($invoice_date));
            $key            = $month."_".$year; 

            // skip anything older than Jan 1 2021
            if($year < 2021) continue; 

            //
            //if(!is_null($invoice->paid_at)) continue; 
            
            if(!array_key_exists($key, $months))
            {
                $rolling_target += $monthly_target;

                $o = new \stdClass();
                $o->services_total = 0.00;
                $o->media_total = 0.00;
                $o->digital_services_total = 0.00; 
                $o->expenses_total = 0.00;
                $o->monthly_target = $monthly_target;
                $o->rolling_target = $rolling_target;
                $o->rolling_total = $rolling_total;
                $o->rolling_difference = 0; 
                $months[$key] = $o;
            }
            


            foreach($invoice->lines as $line)
            {
                
                $invoice_index[$invoice->invoice_number] += $line->amount;

                if($line->kind == "Media")
                {
                    $months[$key]->media_total = $months[$key]->media_total + $line->amount;

                }else if($line->kind == "Services") 
                {
                    $months[$key]->services_total = $months[$key]->services_total + $line->amount;
                    $months[$key]->rolling_total = $months[$key]->rolling_total + $line->amount;
                    $rolling_total += $line->amount;

                }else if($line->kind == "Digital Marketing Services")
                {
                    //if($month == "March") print("<br>Adding ".$line->amount." for ".$invoice->client->name." on invoice ".$invoice->number);
                    $months[$key]->digital_services_total = $months[$key]->digital_services_total + $line->amount;
                    $months[$key]->rolling_total = $months[$key]->rolling_total + $line->amount;
                    $rolling_total += $line->amount;
                    
                }else if($line->kind == "Expenses")
                {
                    $months[$key]->expenses_total = $months[$key]->expenses_total + $line->amount;
                }

                

            }

            $months[$key]->rolling_difference = $rolling_total - $rolling_target ;

            //$rolling_total += $invoice_index[$invoice->invoice_number];
            //$months[$month]->rolling_total;

        }
    }

    public function showInvoices()
    {
        if(env('APP_ENV') == "local")
        {
            setlocale(LC_MONETARY, 'en_US');
        }else
        {   
            setlocale(LC_MONETARY, 'en_US.utf8');
        }
        $invoices = Invoice::orderby("issue_date", 'ASC')->with("lines")->get();
        return view("auth.invoices",["invoices"=>$invoices]);

    }
   
    public function compareMonthsByService($month, $year, $compare_month, $compare_year, $service)
    {


       
        // first we get the invoices for the first month 
        $first_invoices = self::getInvoicesByMonth($month, $year);
        $first_clients = self::filterInvoicesByService($first_invoices, $service);
        $first_month_sum = self::getSumFromFilteredList($first_clients);


        // then we get the invoices for the second month
        $second_invoices = self::getInvoicesByMonth($compare_month, $compare_year);
        $second_clients = self::filterInvoicesByService($second_invoices, $service);
        $second_month_sum = self::getSumFromFilteredList($second_clients);

        $combined = [];

        // now we have two associative arrays with clients as keys and the amounts invoiced for each 
        foreach ($first_clients as $client => $amount) {
            if(array_key_exists($client, $second_clients))
            {
                $combined[$client] = array($amount,$second_clients[$client]);
            }else
            {
                $combined[$client] = array($amount,0);
            }
        }
       
        // now see if there are indexes in the SECOND array that we don't have in our first 
        foreach ($second_clients as $client => $amount) {
            # code...
            if(!array_key_exists($client, $first_clients))
            {
                $combined[$client] = array(0, $amount);
            }
        }

        ksort($combined);


        $dateObj   = \DateTime::createFromFormat('!m', $month);
        $monthName = $dateObj->format('F'); // 
        $dateObj_c   = \DateTime::createFromFormat('!m', $compare_month);
        $compareMonthName = $dateObj_c->format('F'); //

        /// pass this to the view 

        return view('auth.revenue_by_service_month_compare', [
            "month"=>$monthName, 
            "compare"=>$compareMonthName , 
            "clients"=>$combined, 
            "service"=>$service,
            "firstmonthsum"=>$first_month_sum,
            'secondmonthsum'=>$second_month_sum,
            ]);


        
    }


    public function getInvoicesForPL()
    {
        $month = '06';
        $year = '2021';
        $invoices = self::getInvoicesByMonth($month, $year);

        $dateString = '2021-'.$month.'-01';

        //Last date of current month.
        $lastDateOfMonth = date("Y-m-t", strtotime($dateString));
        // start a new PL Batch (this will get altered later)
        
        $batch = new PLBatch;
        $batch->date_end = $lastDateOfMonth;
        $batch->save();

        

        

       
        //TODO:  Need to add in the piece where we figure out what old invoices are still outstanding. 
        //TODO: Sum everything 

        $gross_revenue_for_period = 0.00;
        $total_tax_for_period = 0.00; 
        $dept_totals = [];
        $dept_totals["Creative"] = 0.00; 
        $dept_totals["Performance Marketing"] = 0.00; 
        $dept_totals["Account Management"] = 0.00; 
        $dept_totals["AM/PM"] = 0.00; 
        $dept_totals["Development"] = 0.00; 
        $dept_totals["Project Management"] = 0.00; 
        $dept_totals["Strategy"] = 0.00; 
        $dept_totals["Management"] = 0.00; 
       
        // looks like we don't need this anymore
        $first = $invoices->first()->lines()->get();

        
        $entries_by_project = [];
       
        // for each invoice go and get the related time associated with a project 
        // so where this *might* fall down is if a project is billed out across multiple invoices in a given month. 
        // also what happens if a line is NOT tied to a project ?

        $counter = 0; 
        foreach($invoices as $invoice)
        {
               
          if($counter > 5) continue; 

          $counter++; 

          $lines = []; 

          $invoice_lines = $invoice->lines()->with('project')->get();
          
          $total_expenses_by_project = 0; 

          $i = 1; 

          foreach($invoice_lines as $line)
          {

            // This uses a filter to ONLY get entries we haven't counted in a previous batch
            $time = TimeEntry::where("project_id", $line->project_id)->whereMonth("date_entry", $month)->where("billable", true)->whereNotIn("id",function ($query) {
                                    $query->select('entry_id as id')->from('time_entry_attributed'); })->with("employee")->get();


            //TODO: Determine when an expense has been "counted"
            // grab the expenses related to this project that HAVE NOT YET been counted/ 

            $expenses = HarvestExpense::where("project_id", $line->project_id)->whereNotIn("harvest_expense_id",function ($query) {
                                    $query->select('harvest_id as harvest_expense_id')->from('harvest_expenses_attributed'); })->get();

            // Now we've got a collection of expenses we can mark as "counted"

            $expenses_by_project = [];
            foreach($expenses as $expense)
            {
                $hea = new HarvestExpenseAttributed;
                $hea->pl_batch = $batch->id; 
                $hea->harvest_id = $expense->harvest_expense_id;
                $hea->save();

                // now flatten this out 
                if(array_key_exists($expense->project_id, $expenses_by_project))
                {
                    $expenses_by_project[$expense->project_id] += $expense->total_cost;

                }else
                {
                    $expenses_by_project[$expense->project_id] = $expense->total_cost;
                }

                $total_expenses_by_project += $expense->total_cost;
            }

            // ^^ this is untested. 


           
            // Break out the time by dept 
            $o = [];
            $total_hours = $time->sum('entry'); 
             
            foreach($time as $entry)
            {

              
                $staff = $entry->employee->employee_name; 
                $hours = $entry->entry; 
                $dept  = $entry->employee->dept_name; 

                if(!array_key_exists($dept, $o))
                {
                    $o[$dept] = $hours; 
                }else
                {
                    $o[$dept] += $hours; 
                } 

                // Now that it's been counted... add to time_entry_attributed table 
                $tea = new TimeEntryAttributed; 
                $tea->pl_batch = $batch->id; 
                $tea->entry_id = $entry->id; 
                $tea->save();



            }

            //Determine the ratio of each dept's total contributions to that months work effort 
            $time_and_ratios = []; 
            //$o = array indexed by dept name with hours for each dept (we're still at the project level here)
            foreach($o as $key => $value)
            {

                $ratio = round(($value / $total_hours) * 100); // really could remove the * 100 here son. 
                $details = new \stdClass; 
                $details->hours = $value; 
                $details->percentage = $ratio;
                $details->attribution = $line->amount * ($ratio/100);
                
                $dept_totals[$key] += $details->attribution;

                $time_and_ratios[$key] = $details; 

            }
            

             



            
            $l = new \stdClass; 
            $l->time_entries = $time_and_ratios;
            $l->total_hours =  $total_hours; 
            $l->earned_revenue = $total_hours * $line->project->hourly_rate;
            // TODO: if the earned revenue exceeds that of the amount available on the invoice to be attributed then we need to cap it

            $l->line = $line; 
            
            array_push($lines, $l);
            $i++; 

          }// end investigation by line... 

        
        $financials = new \stdClass;
        $financials->invoice_total = $invoice->total_amount;
        $financials->tax = $invoice->tax;
        $financials->expenses = $total_expenses_by_project;
        $financials->net_amount = $invoice->total_amount - $invoice->tax - $total_expenses_by_project;


        $details = new \stdClass;
        $details->invoice_totals = $financials; 
        $details->issue_date = $invoice->issue_date;
        $details->client = $invoice->client->client_name; 
        $details->harvest_id = $invoice->harvest_id;
        $details->lines = $lines; 

        

        $entries_by_project[$invoice->invoice_number] =  $details;

        }
        
        //return response($entries_by_project)->header("Content-Type", "json"); 

        // get the sum totals

        return view("auth.plreport", [
            "invoices"=>$entries_by_project, 
            "gross"=>$gross_revenue_for_period,
            "dept_totals"=>$dept_totals,
            "total_tax_for_period"=>$total_tax_for_period
        ]);


    }

    private static function filterInvoicesByService($invoices,$service)
    {
       
        if(env('APP_ENV') == "local")
        {
            setlocale(LC_MONETARY, 'en_US');
        }else
        {   
            setlocale(LC_MONETARY, 'en_US.utf8');
        }


        $clients = [];
         // we go through each line and if it matches the $service we are looking for then 
        foreach ($invoices as $invoice) {
            foreach ($invoice->lines as $line) {

                if($line->kind == $service)
                {

                    // get the client of that invoice and shove that as an index (by client name) into an associative array 
                    $client = $invoice->client()->first();
                    if(isset($client->client_name)) 
                        {
                            $client_name = $client->client_name; 
                        }else{
                            $client_name = "Undefiend";
                        }

                    if(!array_key_exists($client_name, $clients))
                    {
                        $clients[$client_name] = $line->amount;
                    }else
                    {
                         $clients[$client_name] += $line->amount;
                    }
                }
            }

        }
        return $clients;

    }

    private static function getSumFromFilteredList($list)
    {
        $sum = 0; 
        foreach ($list as $key=>$amount) {
            $sum += $amount;
        }
        return $sum;
    }

    private static function getInvoicesByMonth($month, $year=2018)
    {
        Log::debug("Getting Invoices for Month ".$month);

        return Invoice::whereMonth('issue_date', $month)->whereYear('issue_date', $year)->with('lines', 'client')->get();
    }
    


    // this should really be offloaded to something other than this Controller. 

    public function makeEmployees()
    {
        //'employee_name', 'email', 'employee_id', 'dept_name'

        // call out to Harvest and get EVERY active person who is found in the API 

        $users = makeAPICall("users", null);
        if($users === FALSE)
        {
            print("API Error");
            return; 
        }

        $users = json_decode($users)->users;
        foreach($users as $user)
        {
            
            //$staff = User::where("email", $user->email)->first(); 
            //if(!$staff)
            //{

                $employee = new Employee;
                $employee->employee_name = $user->first_name." ".$user->last_name;
                $employee->email = $user->email; 
                $employee->employee_id = $user->id;

                // get the role as a proxy for Dept

                $roles = $user->roles;
                if(count($roles) > 0)
                {
                    $employee->dept_name = $roles[0];
                }else 
                {
                    $employee->dept_name = ""; // otherwise just make it in empty string. 
                }

            //}

            print("new employee: ".$employee->employee_name);
            
                    
        
            $employee->save();

        }
    }
}
