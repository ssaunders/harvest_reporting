<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\User;
use App\Client;
use App\HarvestExpense; 
use App\Expense; 
use App\CashBalance; 
use App\Invoice; 
use DB; 
use Log; 


use Kris\LaravelFormBuilder\FormBuilder;


class CashBalanceController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('admin');
    }



   public function create(FormBuilder $formBuilder)
   {
       $form = $formBuilder->create(\App\Forms\CashBalance::class, [
            'method' => 'POST',
            'url' => route('postcashbalance')
        ]);

       return view('auth.cashbalance.create', compact('form'));
   }

    public function store(FormBuilder $formBuilder)
    {
        $form = $formBuilder->create(\App\Forms\CashBalance::class);

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        CashBalance::create($form->getFieldValues());

        // go back to financial outlook 
        return redirect()->route('cashbalance'); 
    }


 



    public function index()
    {

        if(env('APP_ENV') == "local")
        {
            setlocale(LC_MONETARY, 'en_US');
        }else
        {   
            setlocale(LC_MONETARY, 'en_US.utf8');
        }


        // get the latest cashbalance from RBC Checking 
        $opening_balance = CashBalance::where('account', 'RBC CAD Chequing')->orderby('date_updates', 'DESC')->first();
        $tangerine = CashBalance::where('account', 'Tangerine')->orderby('date_updates', 'DESC')->first();
        $waypay = CashBalance::where('account', 'WayPay')->orderby('date_updates', 'DESC')->first();
        $paypal = CashBalance::where('account', 'PayPal')->orderby('date_updates', 'DESC')->first();
        
        $total_cash = $opening_balance->balance+$tangerine->balance+$paypal->balance+$waypay->balance;

        // print "Cash Money: ".$opening_balance->balance;

        $rolling_balance = $opening_balance->balance;

        // now let's get all the invoices that have not been paid yet, but have been issued. 
        $invoices = Invoice::where('status', '=', 'open')->get();

        //print("<br>There are ".count($invoices)." invoices open right now ");

        $total_open_ar  = $invoices->sum("total_amount");
        $total_tax      = $invoices->sum("tax");
        $total_service_fees = $total_open_ar - $total_tax;

        //print("<br>Total amount open:  $".$total_service_fees.". ");

        // and now let's get all known expenses moving forward and that have not been paid
        $expenses = Expense::where('status', 'unpaid')
                            ->orderby("due_date", 'ASC')
                            ->get();

        $forecast_range = 9;
            
        // what is the week of the year right now ?
        $current_week_of_the_year  = (int) date('W');
        
        //$last_weeks_date = getTheWeek(52, false);
        
        //this little bit of uglieness is to handle the fact that you can have a 53rd week in a year...
        //if($current_week_of_the_year == 1 || $current_week_of_the_year == 53) { $current_week_of_the_year = 52 ;}
        

        $last_weeks_date = getTheWeek( $current_week_of_the_year-1, false, 2022);
        
        $target_end_week = $current_week_of_the_year+$forecast_range;

        $rolling_ar = 0.00; 

        $last_weeks_date_for_comparison = date('Y-m-d', strtotime($last_weeks_date->format('Y-m-d')." + 6 days"));
        //print('<br>Late invoices are ones older than  = '.$last_weeks_date->format('Y-m-d'));

        $late_invoices = Invoice::issued()->where('status', '!=', 'paid')->whereDate('due_date', '<', $last_weeks_date_for_comparison)->get();

        $outstanding_ar = $late_invoices->sum('total_amount') - $late_invoices->sum('tax');

        //print("<br>outstanding_ar (i.e 'late') = ".$outstanding_ar);
        
        $rolling_ar += $outstanding_ar; // maybe a slight refactor here 

        // now let's look ahead a bunch (let's say X weeks) to see where our cash expects to be 

        //$rolling_balance  += $late_invoices->sum("total_amount") - $late_invoices->sum("tax"); // assumes we won't get the old debt.... 
        
        $cash_over_time = [];

        //print("<br> current week of the year=".$current_week_of_the_year);

        for($i=$current_week_of_the_year; $i<=$target_end_week; $i++)
        {


            $nextyear = false;
            if($i>52) 
            {
                // if we're crossing into a new calendar year then reset the week num by offsetting 52 weeks 
                $nextyear = true;

                $week_num = $i - 52; 
            }else 
            {
                $week_num = $i; 
            }
            $week = getTheWeek($week_num, $nextyear, 2022); // date obj 


            // get the invoices we expect to be paid this week 
            $start_of_week = $week->format('Y-m-d');
            $end_of_week = date("Y-m-d", strtotime($start_of_week. ' + 6 days'));

            //print("<br> start of the week ".$start_of_week." ::: end of week ".$end_of_week);


            $payments_this_week = Invoice::where('status', '=', 'open')->whereBetween("due_date", [$start_of_week,$end_of_week])->get();



            $invoice_ids = $payments_this_week->pluck("invoice_number"); 

            $net_payment_for_week = $payments_this_week->sum("total_amount") - $payments_this_week->sum("tax");

            //print("<br> payments_this_week-> $".$payments_this_week->sum("total_amount"). " from ". count($payments_this_week). " invoices");

            
            $rolling_ar +=  $payments_this_week->sum("total_amount");

            $this_weeks_expenses = Expense::where('status','unpaid')->whereBetween("due_date", [$start_of_week,$end_of_week])->get();

            $expense_amount = $this_weeks_expenses->sum("amount");


            $rolling_balance += ($payments_this_week->sum("total_amount") - $this_weeks_expenses->sum("amount"));
            $o = new \stdClass();
            $o->gross_payments = $payments_this_week->sum("total_amount");
            $o->expenses = $expense_amount;
            $o->balance = $rolling_balance;
            $cash_over_time[$start_of_week] = $o;

        }

         // print("<br> DOUBLE CHECK:: Rolling AR  (includiung late) is ".$rolling_ar);
// 
           return view('auth.cashbalance.index', ["accounts_receivable"=>$total_service_fees, 
                                                  "late_invoices"=>$late_invoices,
                                                  "cashflow"=>$cash_over_time,
                                                  "total_cash"=>$total_cash]);



    }
    

    public function listBalances()
    {
       
       if(env('APP_ENV') == "local")
        {
            setlocale(LC_MONETARY, 'en_US');
        }else
        {   
            setlocale(LC_MONETARY, 'en_US.utf8');
        }

        $opening_balance = CashBalance::where('account', 'RBC CAD Chequing')->orderby('date_updates', 'DESC')->first();
        $tangerine = CashBalance::where('account', 'Tangerine')->orderby('date_updates', 'DESC')->first();
        $waypay = CashBalance::where('account', 'WayPay')->orderby('date_updates', 'DESC')->first();
        $paypal = CashBalance::where('account', 'PayPal')->orderby('date_updates', 'DESC')->first();
        $accounts = array('tangerine'=>$tangerine->balance,'waypay'=>$waypay->balance, 'paypal'=>$paypal->balance, 'rbc'=>$opening_balance->balance);
        $total_cash = $opening_balance->balance+$tangerine->balance+$paypal->balance+$waypay->balance;
        
        

        return view('auth.cashbalance.list', compact('accounts', 'total_cash')); 
    }

}
