<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\User;
use App\Expense;
use App\Project;
use App\Client;
use App\HarvestExpense;
use App\ExpenseChannel;

use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;

use DB; 
use Log; 



class ExpenseController extends Controller
{
    use FormBuilderTrait;
    
    //
    public function __construct()
    {
        $this->middleware('admin');
    }


    // triggered from a Job to grab all harvest expenses 
    public static function getExpensesFromHarvest()
    {
        DB::table('harvest_expenses')->truncate();
        $expenses = makeAPICall('expenses', null); 
        
        if($expenses === FALSE)
        
        {
            return "API Issue";
        }

        $all = json_decode($expenses);

        for($i=1; $i<=$all->total_pages; $i++)
        {
            $expenses = makeAPICall("expenses?page=".$i, null);
            $all_expenses = json_decode($expenses);
            self::saveExpenses($all_expenses);
        }

        Log::debug("Expense DATA IMPORTED (".$all->total_pages." pages)");


    }

    

     // takes the response from the Harvest API and parses it into the data model 
    private static function saveExpenses($expenses)
    {
  

        foreach($expenses->expenses as $expense)
        {

            $new_expense = new HarvestExpense([
                'harvest_expense_id'=>$expense->id,
                'project_id'=>$expense->project->id,
                'client_id'=>$expense->client->id,
                'staff_id'=>$expense->user->id,
                'expense_category'=>$expense->expense_category->name,
                'total_cost'=>$expense->total_cost,
                'notes'=>$expense->notes,
                'status'=>'',
                'spent_date'=>$expense->spent_date,
                'is_billed'=>$expense->is_billed
                ]);
            
            $new_expense->save();
               
        }
    }


    public function showExpenses()
    {

         if(env('APP_ENV') == "local")
        {
            setlocale(LC_MONETARY, 'en_US');
        }else
        {   
            setlocale(LC_MONETARY, 'en_US.utf8');
        }


        $expenses = HarvestExpense::with('project')->get();

        // get the rest of the business expenses 
        $business_expenses = Expense::where('status','unpaid')->orderby('due_date', 'asc')->with('channel')->get();


        return view('auth.expense_for_business', ["expenses"=>$business_expenses]);

    }




    public function expensesByProject($projectid)
    {
        if(env('APP_ENV') == "local")
        {
            setlocale(LC_MONETARY, 'en_US');
        }else
        {   
            setlocale(LC_MONETARY, 'en_US.utf8');
        }

        $project = Project::where("project_id", $projectid)->first();
        $expenses = HarvestExpense::where('project_id', $projectid)->with('staff')->get();

        return view('auth.expense_by_project', ['expenses'=>$expenses, "projectname"=>$project->project_name]);

    }

    public function expensesByClient($clientid)
    {
        if(env('APP_ENV') == "local")
        {
            setlocale(LC_MONETARY, 'en_US');
        }else
        {   
            setlocale(LC_MONETARY, 'en_US.utf8');
        }

        $client = Client::where("client_id", $clientid)->first();
        $expenses = HarvestExpense::where('client_id', $clientid)->with('project')->get();
        $total_expenses = $expenses->sum('total_cost');
        


        return view('auth.expense_by_client', ['expenses'=>$expenses, 
                                                "clientname"=>$client->client_name, 
                                                "total_expenses"=>$total_expenses]);

    }


    public function createExpenseChannel(FormBuilder $formBuilder)
    {
       $form = $formBuilder->create(\App\Forms\ExpenseChannel::class, [
            'method' => 'POST',
            'url' => route('postexpensechannel')
        ]);

       return view('auth.otherexpense.channel', compact('form'));

    }


    public static function postBusinessExpenseChannel(FormBuilder $formBuilder)
    {
        $form = $formBuilder->create(\App\Forms\ExpenseChannel::class);

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }


        ExpenseChannel::create($form->getFieldValues());

        // go back to financial outlook 
        return redirect()->route('cashbalance'); 
    }



    public static function createBusinessExpense()
    {

         $new_expense = new Expense([
                'channel_id'=>$request->channel_id,
                'due_date'=>$request->due_date, 
                'date_paid'=>null, 
                'status'=>$request->status
        ]);
            
        $new_expense->save();

    }




    public function createOtherExpense(FormBuilder $formBuilder)
    {
       $form = $formBuilder->create(\App\Forms\OtherExpense::class, [
            'method' => 'POST',
            'url' => route('postotherexpense')
        ]);

       return view('auth.otherexpense.create', compact('form'));
    }

    public function editOtherExpense($expense_id)
    {
       
        
       $expense = Expense::find($expense_id);  

       //dd($expense->toArray());

       $form = $this->form(\App\Forms\OtherExpense::class, [
            'method' => 'POST',
            'url' => route('postotherexpense'),
            'model' => $expense,
            'data' => $expense->toArray()
        ]);

       return view('auth.otherexpense.create', compact('form'));
    }



    public function postOtherExpense(FormBuilder $formBuilder)
    {

        $form = $formBuilder->create(\App\Forms\OtherExpense::class);

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $values = $form->getFieldValues();
        
        $expense = new Expense();

        if(array_key_exists("id", $values))
        {
            $expense = Expense::find($values['id']);
            if(!$expense) 
            {

                $expense = new Expense();
            } 
        }

        // is there a more efficient way to do this? 
        $expense->channel_id = $values['channel_id'];
        $expense->amount = $values['amount'];
        $expense->status = $values['status'];
        $expense->due_date = $values['due_date'];
        $expense->date_paid = $values['date_paid'];


        $expense->save();


        // go back to financial outlook 
        return redirect()->route('listexpenses'); 
    }



    // is this beig used?
    public function postCreateNewBusinessExpense(Request $request)
    {

        $new_expense = new HarvestExpense([
                'channel_id'=>$request->channel_id,
                'due_date'=>$request->due_date, 
                'date_paid'=>null, 
                'status'=>$request->status
        ]);
            
        $new_expense->save();

    }


    
}
