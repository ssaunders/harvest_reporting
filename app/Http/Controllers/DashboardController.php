<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\User;
use App\Client;
use App\Invoice;
use App\InvoiceLine;
use DB; 
use Log; 



class DashboardController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function revenueComparisonToCost()
    {
        if(env('APP_ENV') == "local")
        {
            setlocale(LC_MONETARY, 'en_US');
        }else
        {   
            setlocale(LC_MONETARY, 'en_US.utf8');
        }

        $month_of_the_year  = (int) date('n');

        // monthly summaries
        $invoiced_revenue_by_month = [];
        $hours_revenue_by_month = []; 
        $costs_all_depts_by_month = []; 

        $agency_fixed_costs = 800000;

        for($i=1; $i<=$month_of_the_year; $i++)
        {
            $date = \DateTime::createFromFormat('Y-m-d', '2018-'.$i.'-01');

           
            $month = $date->format('F');
            $month_num = $date->format('n');

            $invoiced_revenue_by_month[$month] = 0; 
            // $hours_revenue_by_month[$month] = 0; 
            $costs_all_depts_by_month[$month] = 0; 


            $first_day_of_month = $date->format("Y-m-01");
            $last_day_of_month = $date->format('Y-m-t'); 


            // get the invoices issued for that month --- services only 
            $invoice_ids = Invoice::where('status', "!=", "draft")
                                ->whereBetween("issue_date", [$first_day_of_month, $last_day_of_month])
                                ->pluck("id")
                                ->toArray();

            $monthly_billing_dm = InvoiceLine::whereIn("invoice_id", $invoice_ids)
                                ->where("kind", "Digital Marketing Services")
                                ->sum("amount");
            
            $monthly_billing_services = InvoiceLine::whereIn("invoice_id", $invoice_ids)
                                ->where("kind", "Services")
                                ->sum("amount");

           
            $invoiced_revenue_by_month[$month] = $monthly_billing_services+$monthly_billing_dm;

            // now lets do some granular math on human resources
            $month_days = cal_days_in_month(CAL_GREGORIAN,$month_num, 2018);
           
            for($j=1;$j<=$month_days;$j++)
            {
                $date = getTheDatefromMonthDay($i,$j);
                $theday = $date->format('Y-m-d');
                $dayOfYear = $date->format('z');

                // get the staff who have been active before or equal to this date and are active 
                // note that this is a fairly inefficient call but whatever
                $active_staff_collection = User::where("active", 1)
                ->where('effective_date', "<=", $theday)
                ->where("active", 1)
                ->where("dept", "!=", "")
                ->get();

                // 
                
                // figure out who has LEFT 
                $filtered = $active_staff_collection->filter(function ($record) use ($theday) {
                    

                    // if they don't have an end date on file then they are still working
                    if(is_null($record->end_date))
                    {
                        return true;
                    }
                    
                    // if the end date on file exceeds the start date of this week exclude them. 
                    if($record->end_date < $theday)
                    {
                        return false;
                    }

                    // otherwise keep em
                    return true;
                });

                $active_staff = count($filtered);


                
                $individual_share       = ceil(($agency_fixed_costs/365)/$active_staff); // daily 


                 // get the list of active staff IDS and use it as a filter when calculating salary costs 
                $user_ids = $filtered->pluck("harvest_id");



                $dept_costs = DB::table("users")
                ->select(DB::raw("dept, sum(salary) as dept_cost, sum(agency_extras) as extras, count(harvest_id) as team_count"))
                ->whereIn('harvest_id', $user_ids)
                ->where("dept", "!=", "")
                ->groupBy("dept")
                ->get(); 

                // for storing the costs
                $costs = [];

                foreach($dept_costs as $dept)
                {
                    $daily_dept_salary_cost = ceil($dept->dept_cost/365); // daily rate 
                    $daily_extras_by_dept  = ceil($dept->extras/365); // daily rate
                  

                    $daily_share_of_agency = $individual_share  * $dept->team_count; // <-- needs to be by team 
                    $dept_expense           = $daily_dept_salary_cost + $daily_extras_by_dept +  $daily_share_of_agency; 
                    
                    $costs_all_depts_by_month[$month]  += $dept_expense; // by the week, for ALL depts
                    
                    //$costs[$dept->dept]     = $dept_expense; // by the week, for one dept
                }

            }

        }
        
        return view("auth.dashboard", ["costs_by_month"=>$costs_all_depts_by_month, 
                                        "invoiced_revenue_by_month"=>$invoiced_revenue_by_month]);

       

    }
    
}
