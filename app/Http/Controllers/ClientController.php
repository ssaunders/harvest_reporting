<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\User;
use App\Client;
use App\HarvestExpense; 
use DB; 
use Log; 



class ClientController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('admin');
    }



    public static function getClients()
    {
        DB::table('clients')->truncate();
        $clients = makeAPICall('clients', null); 
        
        if($clients === FALSE)
        
        {
            return "API Issue";
        }

        $all = json_decode($clients);

        for($i=1; $i<=$all->total_pages; $i++)
        {
            $clients = makeAPICall("clients?page=".$i, null);
            $all_clients = json_decode($clients);
            self::saveClients($all_clients);
        }

        Log::debug("CLIENT DATA IMPORTED (".$all->total_pages." pages)");


    }

    public function showClients()
    {
         if(env('APP_ENV') == "local")
        {
            setlocale(LC_MONETARY, 'en_US');
        }else
        {   
            setlocale(LC_MONETARY, 'en_US.utf8');
        }

        $service_revenue =[];
        $expenses = [];


        $clients = Client::orderBy('client_name')
                        ->withCount('projects')
                        ->withCount('issuedInvoices')
                        ->with('issuedInvoices', 'issuedInvoices.serviceFeeLines')
                        ->with('projects')
                        ->with('expenses')
                        ->get();

        /* 
            now we have this issue in that we want to effective rates 
            but this is only relevant in THIS year because we only started 
            using harvest this year for invoicing

            so we have to get project hours from THIS year
        */ 

        //dd($clients->toArray());

        /*  
            haven't figured out a way to sum the relevant line items 
            in the child models of invoices (InvoiceLines) where there 
            are service fees only.... so we gonna extract them a bit less elegantly
        */
        foreach ($clients as $client) 
        {
            $service_revenue[$client->client_id] = 0; 
            foreach ($client->issuedInvoices()->get() as $invoice) 
            {
                $service_revenue[$client->client_id] += $invoice->serviceFeeLines()->sum("amount");
            }
        }
        

        foreach ($clients as $client) 
        {
            $expenses[$client->client_id] =$client->expenses->sum('total_cost');
        }


        return view("auth.clients", ["clients"=>$clients, "service_revenue"=>$service_revenue, "expenses"=>$expenses]);
    }
    

     // takes the response from the Harvest API and parses it into the data model 
    private static function saveClients($clients)
    {
        // make an invoice
        $client_count = 0; 

        foreach($clients->clients as $client)
        {

            $new_client = new Client([
                "client_id"=>$client->id,
                "client_name"=>$client->name,
                "is_active"=>$client->is_active,
                "currency"=>$client->currency,
                ]);
            
            $new_client->save();
            
            $client_count++;
        }

        Log::debug($client_count." clients have been saved");


    }


    
}
