<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Project;
use App\TimeEntry;
use App\User;
use App\Invoice;
use App\InvoiceLine;
use App\ApplicationNotice;
use Config;
use Log; 




class HarvestController extends Controller
{
    //

   

    


    public function generateWeeklyCostsAndRevenue()
    {
          
        if(env('APP_ENV') == "local")
        {
            setlocale(LC_MONETARY, 'en_US');
        }else
        {   
            setlocale(LC_MONETARY, 'en_US.utf8');
        }
          // what is the week of the year right now ?
          $week_of_the_year  = (int) date('W');

          $agency_fixed_costs = 800000;

          // factor non-billables into the agency fixed costs 
          $non_billables = User::whereNotNull("non_billable_percentage")->get();
          foreach ($non_billables as $non_billable_user) {
              $salary = $non_billable_user->salary + $non_billable_user->agency_extras; 
              $attribute_to_agency = ($salary) * ($non_billable_user->non_billable_percentage/100);
              $agency_fixed_costs += $attribute_to_agency;
              Log::debug("agency_fixed_costs = ". $agency_fixed_costs);
          }



          /// $agency_fixed_costs = 800000;

          $cost_and_revenue = [];
          $hours_revenue_by_month = []; 
          $ytd_costs_and_revenue_by_dept = []; // dept P&L

          
          // generate a simple associative lookup to get Staff's harvest IDs in the view
          $staff_ids = User::pluck('harvest_id', 'name');


           // get an index of projects and their bill rates 
            $projects = Project::all();
            $project_rates = $projects->pluck('effective_rate', 'project_id'); // [];
          

          for($i=$week_of_the_year; $i>=1; $i--)
          {
                $date = getTheWeek($i, true);
                $week_start_date =  $date->format('Y-m-d');
                $end_of_week = date("Y-m-d", strtotime($week_start_date. ' + 6 days'));
                $month = $date->format('F');
               
                // get the staff who have been employed before or equal to this date and are active and belong to a dept
                $filtered = getStaffForWeek($i); // from helpers.php

                $active_staff = count($filtered);
              
                $individual_share       = ceil(($agency_fixed_costs/52)/$active_staff);  // for the week
                
                Log::debug("Individual share of agency cost = ".$individual_share);


                $total_weekly_expense = 0; 

              
                $dept_costs = [];
                $costs_by_employee = [];
                foreach ($filtered as $staff) {
                   
                    // accomodate non-billable staff

                    //$staff_cost = (($staff->salary*((100 - $staff->non_billable_percentage)/100) + $staff->agency_extras) / 52) + $individual_share; // for the week
                    $staff_cost = (($staff->salary + $staff->agency_extras) / 52) + $individual_share; // for the week
                    $costs_by_employee[$staff->dept][$staff->name] = $staff_cost; 

                    if(!array_key_exists($staff->dept, $dept_costs))
                    {
                         $o = new \stdClass();
                         $o->dept = $staff->dept;
                         $o->dept_cost = $staff->salary;
                         $o->extras = $staff->agency_extras;
                         $o->team_count = 1; 
                         $dept_costs[$staff->dept] = $o; 
                         continue;
                    }

                    $o = $dept_costs[$staff->dept];

                    $o->dept_cost += $staff->salary;
                    $o->extras += $staff->agency_extras;
                    $o->team_count += 1; 
                    $dept_costs[$staff->dept] = $o; 

                   

                }


                // for storing the costs
                $costs = [];
                $target_hours = [];
                $billable_team_count = [];




                // get the revenue by timesheets 
                $revenue = self::revenueByWeekNumber($i,$project_rates); //<-- based on hours worked and effective rate by project
                
                $revenue_for_week = $revenue->revenue_by_dept;
                $revenue_by_employee = $revenue->revenue_by_employee; 
                $hours_earned_by_dept = $revenue->hours_earned_by_dept; 
                $hours_by_dept_by_employee = $revenue->hours_by_dept_by_employee;


                foreach($dept_costs as $dept)
                {
                    // order the dept details by name 
                    if(array_key_exists($dept->dept, $costs_by_employee))
                    {
                        $staff_array = $costs_by_employee[$dept->dept];
                        ksort($staff_array);
                        $costs_by_employee[$dept->dept] = $staff_array;    
                    }
                    
                    if(array_key_exists($dept->dept, $revenue_by_employee))
                    {
                        $revenue_staff_array = $revenue_by_employee[$dept->dept];
                        ksort($revenue_staff_array);
                        $revenue_by_employee[$dept->dept] = $revenue_staff_array;
                    }
                    
                    if(array_key_exists($dept->dept, $hours_by_dept_by_employee))
                    {
                        $hours_staff_array = $hours_by_dept_by_employee[$dept->dept];
                        ksort($hours_staff_array);
                        $hours_by_dept_by_employee[$dept->dept] = $hours_staff_array;
                    }

                    

                    $weekly_dept_salary_cost = ceil($dept->dept_cost/52);
                    $weekly_extras_by_dept  = ceil($dept->extras/52);
                  

                    $weekly_share_of_agency = $individual_share  * $dept->team_count; // <-- needs to be by team 
                    $dept_expense           = $weekly_dept_salary_cost + $weekly_extras_by_dept +  $weekly_share_of_agency; 
                    
                    $hours_per_person       = ($dept_expense / $dept->team_count) / 165; // we don't use this currently 
                   

                    $total_weekly_expense   +=  $dept_expense; // by the week, for ALL depts
                
                    $costs[$dept->dept]     = $dept_expense; // by the week, for one dept

                    $hours_to_target        = ceil(($dept_expense) / 165); 
                    $target_hours[$dept->dept] = $hours_to_target;

                }


                /*
                
                 Flattens the arrays into a single one with the same keys, 
                 - where first dimension is the costs,
                 - the second is the revenue
                 - third is the hours to target at an assumed rate of 165
                 - fourth is the actual hours we have generated to date 

                */
                // print("<pre>");
                // print_r($hours_by_dept_by_employee);
                // print("</pre>");


                // let's do a quick and dirty YTD P&L for each Dept: 
                foreach ($costs as $dept => $cost) 
                {
                    if(!array_key_exists($dept, $ytd_costs_and_revenue_by_dept)) 
                    {
                        $o = new \stdClass();
                        $o->costs = 0; 
                        $o->revenue = 0; 
                        $ytd_costs_and_revenue_by_dept[$dept] = $o; 
                    }

                    $pl = $ytd_costs_and_revenue_by_dept[$dept];
                    $pl->costs += $cost; 
                    // little shortcut to get the revenue while we're at it 
                    if(array_key_exists($dept, $revenue_for_week))
                    {
                        $pl->revenue += $revenue_for_week[$dept];
                    }

                    $ytd_costs_and_revenue_by_dept[$dept] = $pl;

                } 

                 
                $costs_vs_revenues = array_merge_recursive($costs, $revenue_for_week, $target_hours, $hours_earned_by_dept);

                $total_weekly_revenue = 0; 
               
                foreach($revenue_for_week as $dept=>$total)
                {
                    $total_weekly_revenue += $total;

                }

                $total_hours_for_week = 0; 

                foreach ($hours_earned_by_dept as $dept=>$hours) 
                {
                    $total_hours_for_week += $hours; 
                }

                // did we cover costs by revenue hours
                $percent_to_target = floor(($total_weekly_revenue / $total_weekly_expense ) * 100);



                $cost_and_revenue[$month][$week_start_date] = [
                                    "costs_and_revenues"=> $costs_vs_revenues, 
                                    "total_cost"=> $total_weekly_expense, 
                                    "total_revenue"=>$total_weekly_revenue,
                                    "total_hours" =>$total_hours_for_week,
                                    "percent_to_target"=>$percent_to_target,
                                    "revenue_by_employee" => $revenue_by_employee,
                                    "costs_by_employee"=>$costs_by_employee,
                                    "hours_by_dept_by_employee"=>$hours_by_dept_by_employee]; 
                


          }// end that week


          
          return view("auth.weekly_costs_and_revenue", ["all_weeks"=>$cost_and_revenue, "staff_ids"=>$staff_ids, "ytd_pl"=>$ytd_costs_and_revenue_by_dept]);

    }

  

    // new method to only grab what we don't have 
    // allows for selecting only by project or by client (may flesh that out later)

    public static function pollForNewProjectData($active_projects=true, $project_name=null, $client_name=null)
    {

        Log::debug("polling for new project data.");

        $filter = "";

        if(!$active_projects)
        {
            Log::debug("THIS IS THE NUCLEAR OPTION PEOPLE");
            DB::table('project_data')->truncate();
            DB::table('time_entries')->truncate();
        }
        

        $call = makeAPICall("projects", null);
        
        if($call === FALSE)
        {
            return "API Error";
        }

        $all_projects = json_decode($call);

        Log::debug("There are ".$all_projects->total_pages." pages of projects.");

        for($i=1;$i<=$all_projects->total_pages; $i++) 
        {
            Log::debug("Getting Project Page $i ");
            $projects = makeAPICall("projects?page=".$i, null);
            $p = json_decode($projects);
            // TODO: Add in the filters as passed to the method
            foreach($p->projects as $project)
            {
                Log::debug($project->name." :: status =".$project->is_active);
                // active project filter
                if($active_projects)
                {
                   if(!$project->is_active)
                    {
                        Log::debug("Skipping Project ".$project->name. " for inactivity.");
                        continue;  
                    }   
                }
                
                self::updateProjectData($project);
            }

            //self::sortProjectsIntoArrays($client_projects, $internal_projects,$vet_strategy_projects,$recurring_projects,$project_ids ,$p);
        }

         Log::debug("Project Data Polling Complete! ");
         self::updateVetStratCalculations();
         self::updateProjectHours();
         Log::debug(" Hours and effective rate calculations complete");


    }

    public static function updateProjectData($project)
    {
        $type = self::getProjectType($project);
        
        $p = Project::where('project_id', $project->id)->first();

        if(!$p)
        {
            $p = new Project();
            Log::debug("creating new project for ".$project->name);
            $p->project_id = $project->id;
        }
        
        $p->project_name=$project->name;
        $p->client_id=$project->client->id;
        $p->is_active=$project->is_active;
        $p->client_name=$project->client->name;
        $p->project_type=$type;

        $budget = 0; 

        // this part is a bit funny when considering the different project types 
        if($project->budget_by == "project" && is_null($project->cost_budget) )//used for time & materials with a cap of hours
        {
            $budget = $project->hourly_rate * $project->budget;

        } else if($project->budget_by == "none") // used for time & materials (need to validate)
        {
            $budget = 0.00;

        }else if($project->budget_by == "project_cost") // used for 'fixed fee' projects
        {
            $budget = $project->cost_budget;
        }else if($project->budget_by == "task")
        {
            $budget = $project->fee;

        }

        $p->project_budget = $budget;
        $p->hourly_rate = $project->hourly_rate;

        // silly little hack for projects that are 'Track by Task'
        if(is_null($project->hourly_rate))
        {
            // we're goiing to assume the rate should be 165/hr
            $p->hourly_rate = 165;
        }

        Log::debug("Attempting to save ".$p->project_name);
        
      
        
        // now to fill in the last 3 project fields (project hours, effective rate, and last_time_entry) we need the timesheets

        // this method also saves the timesheet data 
        $total_hours = self::getTimeSheetDataForProject($project->id);
        Log::debug("project has $total_hours hours");

        
        $p->last_time_entry = self::getLastTimeEntry($project->id);

        $p->save();

        Log::debug($p->project_name. " saved ");

    }

    private static function getProjectType($project)
    {
        $notes = $project->notes;
        $is_recurring = false;
        
        if(strstr($notes, "RECURRING") !== FALSE)
        {
            $is_recurring = true;
        }

        

        if($project->client->name == 'Art & Science')
        {
            return "internal";
        }
        else if($project->client->name == 'VetStrategy')
        {
            return "vet strat";
        }
        else if($is_recurring)
        {
            return "recurring";
        }
        
        // if non of the above then just a "client"
        return "client";
    }

    private static function getTimeSheetDataForProject($projectid)
    {
        
        // get the date going back 4 weeks ago
        $d2 = date('Y-m-d', strtotime('-28 days'));

        $time_limit = "&from=".$d2;
        $time_entries = [];

       
        // TODO: Accomodate things like Vet Strat that started before the calendar year

        $url = 'time_entries?project_id='.$projectid.$time_limit;
        $project_time_results = makeAPICall($url, null);

        if($project_time_results  === FALSE)
        {
           Log::debug("<br>API Error");
           return false;
        }

        // blow away existing records for THIS project and from THIS timeframe 
        $deletedRows = TimeEntry::where('project_id', $projectid)
                                    ->where('date_entry', '>=',$d2)
                                    ->delete();
       
        for($i=1;$i<=json_decode($project_time_results)->total_pages; $i++) 
        {
   
            Log::debug("Getting page $i of time entries  from API for project ".$projectid);
            
            //TODO:  this is repeated... offload to singular function 
            $url = 'time_entries?project_id='.$projectid."&page=".$i.$time_limit;


            $te = makeAPICall($url, null);
            self::formatTimeEntries($time_entries, $te);

        }

        // total the time 
        $total_hours = self::calculateTotalTime($time_entries); 
        
        // save the hours
        self::saveTimeEntries($time_entries); 

        return  $total_hours;
    }

    public static function updateVetStratCalculations()
    {

        $all_time = TimeEntry::where("project_id", "14935197")->sum("entry");
        
        Log::debug("there are currently ".$all_time." hours logged against VS");


        // get all teh vet strat projects that ARE NOT the corp website and business Development that are also active 
        $vs_projects = Project::where("is_active", "1")
                                ->where("project_type", "vet strat")
                                ->where("project_id", "!=", "16343382") // business development 2018
                                ->where("project_id", "!=", "16075330") // recruitment 
                                ->where("project_id", "!=", "15373635") // videos
                                ->where("project_id", "!=", "15339821") // server migration 
                                ->where("project_id", "!=", "14935197") // main VS project 
                                ->where("project_id", "!=", "16777498") // corporate
                                ->get();

        Log::debug("there are ".count($vs_projects). " that are in the active category ");

        $project_ids = $vs_projects->pluck("project_id");                                 

        $entries = TimeEntry::whereIn("project_id", $project_ids)->update(["project_id"=>"14935197"]);

        // foreach ($entries as $entry) {
        //     $new_entry = $entry->replicate();
        //     $new_entry->id = $entry->id.'14935197';

        //     $new_entry->project_id = 14935197;
        //     $new_entry->save();
        // }

        $all_time = TimeEntry::where("project_id", "14935197")->sum("entry");

        Log::debug("there is now  ".$all_time." hours logged against VS");

        $project = Project::where('project_id', "14935197")->first();
        $budget = $project->project_budget; 
        $effective_rate = $budget/$all_time; 
        $project->effective_rate = $effective_rate;
        $project->save();
    }

    public static function updateProjectHours()
    {
        $projects = Project::all();
        Log::debug("Updating ".count($projects)." effective rates");
        foreach ($projects as $project) 
        {
            $time = TimeEntry::where("project_id", $project->project_id)->sum("entry");
            $project->project_hours = $time; 

            if( $project->project_budget == 0) // it's a T&M project 
            {
                $effective_rate = $project->hourly_rate;
            }else
            {
                if( $time > 0 )
                {
                    $effective_rate = $project->project_budget/$time;    
                }else
                {
                    $effective_rate = $project->hourly_rate;
                }   
            }
            
            if($effective_rate > $project->hourly_rate)
            {
                $effective_rate = $project->hourly_rate;
            }
               
            $project->effective_rate = $effective_rate; 
            $project->save();
        }

        Log::debug("project effectives and hourlys updated");
    }



    public static function getLastTimeEntry($projectid)
    {
        $entry = TimeEntry::where("project_id", $projectid)->orderby("date_entry", "DESC")->first();
        
        if(!$entry) return NULL; 

        return $entry->date_entry;

    }


    public function projectPageView(Request $request,$type="client")
    {
        if(env('APP_ENV') == "local")
        {
            setlocale(LC_MONETARY, 'en_US');
        }else
        {   
            setlocale(LC_MONETARY, 'en_US.utf8');
        }

        $showarchives = $request->input('showarchives');

        

        
        if(is_null($showarchives)) $showarchives = false; 
        
        // this is a little funny but apparently necessary to use a conditional clause 
        if(!$showarchives || $showarchives == "false")
        {
            $show_active = true;
        }else{
            $show_active = false;
        }        

        
        $project_list = Project::where("project_type", $type)
                        ->when($show_active, function($query){
                            return $query->where("is_active", "1");
                        })->orderBy("client_name")
                        ->with('clientInvoices', 'clientInvoices.lines')
                        ->with('expenses')
                        ->get();



        foreach($project_list as $project)
        {
            $project_billed = 0; 

            foreach($project->clientInvoices as $invoice)
            {
                if($invoice->status == "draft") continue;

                foreach ($invoice->lines as $line) 
                {
                   if($project->project_id == $line->project_id && $line->kind != "Media" && $line->kind != "Expenses"  )
                   {
                         $project_billed += $line->amount;
                   }
                }

            }
            
            $project->billed = $project_billed;
            $hourly_rate_by_billings = 0; 
            
            if($project_billed > 0 && $project->project_hours > 0)
            {
                $hourly_rate_by_billings  = ($project_billed / $project->project_hours);
            }
            
            $project->invoice_hours = $hourly_rate_by_billings;

            $left_to_bill = $project->project_budget - $project_billed;
            $project->left_to_bill = $left_to_bill;
        }

        return view("auth.projects", ["project_list"=>$project_list, "type"=>$type, "showarchives"=>$showarchives]);
    }


    public function vetStrategyProjectView($project_list)
    {

        $exclusionary_project_ids = [16824303, 16777498, 16075330]; //Daubigny // Vet Strat Corp // Vet Strat Recruitment 
        $primary_project_id = 14935197;

        // we need to figure out how much TOTAL budget there is allocated based on the individual 


        return view("auth.projects_vet_strat", ["project_list"=>$project_list, "type"=>$type]);
    }


   
    private static function saveTimeEntries($entries)
    {
        foreach ($entries as $entry) {
       
            try
            {
                 $time = new TimeEntry([
                    'project_id'=>$entry->project->id, 
                    'id'=>$entry->id, 
                    'employee_id'=>$entry->user->id, 
                    'employee_name'=>$entry->user->name, 
                    'entry'=>$entry->hours, 
                    'date_entry'=>$entry->spent_date, 
                    'billable'=>$entry->billable, 
                    'bill_rate'=>$entry->billable_rate, 
                    'notes'=>$entry->notes, 
                    'task_name'=>$entry->task->name, 
                    'task_id'=>$entry->task->id]);
                 
                 $time->save();
                

            }catch (\Exception $e)
            {
                Log::debug($e->getMessage());
            }   

           
        }

        return true; 
    }



    // new revenue by dept method that uses stored time_sheet data 
    // first walk through all the time records from the week in question 
    // then check each entry against the project effective rate in the project_data table 
    public function revenueByWeekNumber($week_number=1, $project_rates)
    {
        if(env('APP_ENV') == "local")
        {
            setlocale(LC_MONETARY, 'en_US');
        }else
        {   
            setlocale(LC_MONETARY, 'en_US.utf8');
        }

        $date = getTheWeek($week_number);
        $this_week = $date->format('Y-m-d'); 
        $end_of_week = date("Y-m-d", strtotime($this_week. ' + 6 days'));

        $week_of_the_year  = (int) date('W'); // this is THIS week

        $entries = TimeEntry::whereBetween('date_entry', [$this_week, $end_of_week])->get();

        // staff and dept association 
        $staff_by_dept = [];
        $staff = User::where("active", 1)->get();


        foreach($staff as $employee)
        {
            
            if($employee->dept == "") continue;

            $staff_by_dept[$employee->harvest_id] = $employee->dept; 

        }
   

        $total_earned_revenue = 0; 

        $revenue_by_employee = [];
        $revenue_by_dept    = [];
        $hours_earned_by_dept = [];
        $hours_by_dept_by_employee = [];

        foreach ($entries as $entry) 
        {
            if($entry->billable) 
            {
                $amount = $entry->entry * $project_rates[$entry->project_id];
                // Log::debug("-- Finding ".$entry->entry." for ". $entry->employee_id ." at ".$entry->date_entry. " bla blah of time at a rate of ".$project_rates[$entry->project_id]. "for a total of ".money_format('%1n', $amount));

                $total_earned_revenue += $amount;


                $hours_earned_by_dept = addToArrayByKeyWithValue($hours_earned_by_dept, $staff_by_dept[$entry->employee_id], $entry->entry );

                // gotta check 2 keys now...

                if(array_key_exists($staff_by_dept[$entry->employee_id], $revenue_by_employee))
                {
                    // we have a dept 
                    if(array_key_exists($entry->employee_name, $revenue_by_employee[$staff_by_dept[$entry->employee_id]]))
                    {
                        $revenue_by_employee[$staff_by_dept[$entry->employee_id]][$entry->employee_name] += $amount; 
                        $hours_by_dept_by_employee[$staff_by_dept[$entry->employee_id]][$entry->employee_name] += $entry->entry;
                    }else
                    {
                        $revenue_by_employee[$staff_by_dept[$entry->employee_id]][$entry->employee_name] = $amount; 
                        $hours_by_dept_by_employee[$staff_by_dept[$entry->employee_id]][$entry->employee_name] = $entry->entry;
                    }
                }else // no dept key yet
                {
                    $revenue_by_employee[$staff_by_dept[$entry->employee_id]] = [];
                    $hours_by_dept_by_employee[$staff_by_dept[$entry->employee_id]]= [];

                    $revenue_by_employee[$staff_by_dept[$entry->employee_id]][$entry->employee_name] = $amount; 
                    $hours_by_dept_by_employee[$staff_by_dept[$entry->employee_id]][$entry->employee_name] = $entry->entry;
                }

                

                if(array_key_exists($staff_by_dept[$entry->employee_id], $revenue_by_dept))
                {
                    $revenue_by_dept[$staff_by_dept[$entry->employee_id]] += $amount;
                }else
                {
                    $revenue_by_dept[$staff_by_dept[$entry->employee_id]] = $amount;
                }
            }
        }

        $o = new \stdClass();
        $o->revenue_by_dept = $revenue_by_dept;
        $o->revenue_by_employee = $revenue_by_employee;
        $o->hours_earned_by_dept = $hours_earned_by_dept;
        $o->hours_by_dept_by_employee = $hours_by_dept_by_employee;

        
        return $o;
        

    }


 

  

    private static function calculateTotalTime($time_entries)
    {
        $total_time = 0; 

        foreach ($time_entries as $log) {
            $total_time += $log->hours;    
        }

        return $total_time;

    }

    private static function formatTimeEntries(&$data, $input)
    {
       $time_entries = json_decode($input);
       foreach ($time_entries->time_entries as $log) {
           array_push($data, $log);
       }

    }


    

}
