<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\User;
use Log;




class StaffController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('admin');
    }

    public static function getStaffFromHarvest()
    {


    	$users = makeAPICall("users", null);
    	if($users === FALSE)
    	{
    		print("API Error");
    		return; 
    	}

    	$users = json_decode($users)->users;
    	foreach($users as $user)
    	{
    		$date_active = date('Y-m-d', strtotime($user->created_at));
    		
    		$staff = User::where("email", $user->email)->first(); 
    		if(!$staff)
    		{
    			$staff = new User;
    			$staff->name = $user->first_name." ".$user->last_name;
    			$staff->email = $user->email; 
    			$staff->password = bcrypt('1234567890');
    			$staff->salary = 0.00;
    			$staff->agency_extras=0.00;
    			$staff->contractor = $user->is_contractor; 
    			$staff->active = $user->is_active; 
    			$staff->effective_date = $date_active;
    			$staff->harvest_id = $user->id;

    		}else
    		{
    			$staff->harvest_id = $user->id; 
    			$staff->effective_date = $date_active;
    			//$staff->active = $user->is_active; 
    		}
    		
    		   		
    	
    		$staff->save();

    	}

    	

    }

    public function showAll()
    {
    	//self::getStaffFromHarvest();


    	$staff = User::where("active", 1)->get();




    	return view('auth.staff_list', ["staff"=>$staff]);
    }

    public static function  generateHourlyRateCost()
    {
        // grab all the staff 
        $active_staff_collection = User::where("active", 1)->get();
        $date = getTheWeek();
        $this_week = $date->format('Y-m-d');

        // filter out those without active work time right now 
        $filtered = $active_staff_collection->filter(function ($record) use ($this_week) {
                                    
                if($record->non_billable_percentage == "100")
                {
                    Log::debug('Removing '.$record->name." from the active staff collection");
                    return false;
                }

                // if they don't have an end date on file then they are still working
                if(is_null($record->end_date))
                {
                    return true;
                }
                

                
                // if the end date on file exceeds the start date of this week exclude them. 
                if($record->end_date < $this_week)
                {
                    return false;
                }

                
                // otherwise keep em
                return true;
            });


        // store the staff count that is billable 
        $staff_head_count = count($filtered);

        // now get the staff that are not-billable 
        $non_billable_staff         = User::where("non_billable_percentage", '>', 0)->get();
        $non_billable_salary_cost   = 0; 

        foreach ($non_billable_staff as $user) 
        {
            $non_billable_salary_cost += ($user->salary + $user->agency_extras);
        }

        $agency_fixed_costs = 800000; // based on budgets 

        $total_share_of_agency = $agency_fixed_costs + $non_billable_salary_cost;
        $individual_share_of_agency = $total_share_of_agency / count($filtered);



        $workable_days = 52 * 5;

        // how many billable hours a day can we expect to get         
        $utilization_rate = 0.62;
        $number_of_hours_per_day = ceil(8 * $utilization_rate);

        // track how many billable hours we expect to generate 
        $total_billable_hours  = 0; 
        foreach ($filtered as $user) {

            // what are the working days this person can work
            $staff_bill_days = $workable_days - $user->non_working_days;
            $billable_hours = $staff_bill_days * $number_of_hours_per_day;
            $total_billable_hours += $billable_hours;
            // store the billable hours 
            $user->billable_hours = $billable_hours;

            // now take their salary + extras and divide by the number of billable hours 
            $hourly_base_cost = ($user->salary + $user->agency_extras)/$billable_hours;
            $user->base_hourly_cost_rate = $hourly_base_cost;

            // determine the markup based on hours available to earn it 
            $user->hourly_cost_rate =  $hourly_base_cost + ($individual_share_of_agency /  $billable_hours);      
            $user->save();
        }
       

       Log::debug("generateHourlyRateCost COMPLETE!");
       print("generateHourlyRateCost COMPLETE!");

    }
}
