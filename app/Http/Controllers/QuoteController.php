<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\User;
use App\Quote;
use App\QuotePhase;
use App\QuotePhaseLine;
use Auth;
use Log;
use DB; 



class QuoteController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('admin');
    }



   
    public function blankQuote()
    {
        $today = date('Y-m-d');
        $users = User::where('active', 1)->where('effective_date', '<', $today);

        $deals = self::getPipedriveDeals();
        $edit  = false; 


        if(count($deals) == 0 ) return "no pipedrive!"; 

        
        
        return view('auth.quotes.index', ['staff'=>$users, 'deals'=>$deals, 'edit'=>$edit]);
        

    }

    public function listQuotes()
    {
        if(env('APP_ENV') == "local")
        {
            setlocale(LC_MONETARY, 'en_US');
        }else
        {   
            setlocale(LC_MONETARY, 'en_US.utf8');
        }

        // this little bit basically says that we'll only display the most recent version of a quote
        $quote_ids = Quote::select('pipedrive_id', 'id')->orderBy('updated_at', 'DESC')->get();
        $filtered_ids = $quote_ids->unique('pipedrive_id');
        $filtered_ids = $filtered_ids->pluck('id');

        

        $quotes = Quote::wherein('id', $filtered_ids)->with('staff','phases', 'phases.lines', 'phases.phaseCostsLow', 'phases.phaseCostsHigh')->get();


    
        $total_active_quotes_high = 0; 
        $total_active_quotes_low = 0; 
        


        foreach ($quotes as $quote_id => $quote) 
        {
            $jobCostHigh = 0; 
            $jobCostLow  = 0; 

            foreach ($quote->phases as $phase) 
            {
                $jobCostHigh +=  $phase->phaseCostsHigh->first()->cost; 
                $jobCostLow += $phase->phaseCostsLow->first()->cost;
            }

            $quote->jobCostLow = $jobCostLow;
            $quote->jobCostHigh = $jobCostHigh;
            $total_active_quotes_high += $jobCostHigh;
            $total_active_quotes_low += $jobCostLow;

           

        }

        $quote_count = count($quotes);


        return view('auth.quotes.list', compact('quotes', 'total_active_quotes_low', 'total_active_quotes_high','quote_count'));
    }


    public function details($id)
    {
        
        if(env('APP_ENV') == "local")
        {
            setlocale(LC_MONETARY, 'en_US');
        }else
        {   
            setlocale(LC_MONETARY, 'en_US.utf8');
        }
        
        $quote = Quote::where("id",$id)->with('phases', 'phases.lines', 'phases.phaseCostsLow', 'phases.phaseCostsHigh')->first();

        $jobCostHigh = 0; 
        $jobCostLow  = 0; 
        // do a little leg work here
        foreach ($quote->phases as $phase) 
        {
            $phase->phase_cost_high =  $phase->phaseCostsHigh->first()->cost; 
            $phase->phase_cost_low = $phase->phaseCostsLow->first()->cost;
            $jobCostHigh +=  $phase->phaseCostsHigh->first()->cost; 
            $jobCostLow += $phase->phaseCostsLow->first()->cost;
        }

        $quote->jobCostLow = $jobCostLow;
        $quote->jobCostHigh = $jobCostHigh;

        return view('auth.quotes.detail', compact('quote'));
    }

    public function edit($id)
    {
       
        $quote = Quote::where("id",$id)->with('phases', 'phases.lines', 'phases.phaseCostsLow', 'phases.phaseCostsLow')->first();

        $edit = true; 
        $deals = self::getPipedriveDeals();


        return view('auth.quotes.index', compact('quote', 'edit', 'deals'));
    }



    public function saveQuoteDetails(Request $request)
    {
      

        $inputs = $request->all();

        // print("<pre>");
        // print_r($inputs);
        // return;

        // not elegant but whatever -- get the deals from Pipedrive

        $deals = self::getPipedriveDeals();
      
        // this is just fucking stupid but i'm tired so WTF. 
        if(array_key_exists("pipedrive_deal_id", $inputs))
        {
            // this is an update to an existing quote
            // Store the quote ID to retire it and version the quote 
            $pipedrive_deal_id = $inputs['pipedrive_deal_id'];
        }else
        {
            $pipedrive_deal_id = $inputs['pipedrive_deal'];
            
        }

        $deal_name = $deals[$pipedrive_deal_id];

        $phase_names = $inputs['phase'];
        $phase_hours_seo = $inputs['hours_seo'];
        $phase_description_seo = $inputs['description_seo'];

        $phase_hours_creative = $inputs['hours_creative'];
        $phase_description_creative = $inputs['description_creative'];

        $phase_hours_development = $inputs['hours_development'];
        $phase_description_development = $inputs['description_development'];
        
        $phase_hours_pm = $inputs['hours_pm'];
        $phase_description_pm = $inputs['description_pm'];

        $phase_start_date = $inputs['phase_start_date'];
        $phase_end_date = $inputs['phase_end_date'];

        $phase_contingency_description = $inputs['description_contingency'];


        // contingency recalculation -- server side stylez... never implemented... 


        $quote = new Quote([
                                'description'=>"Quote for ".$deal_name,  
                                'staff_id'=>Auth::user()->id,
                                'pipedrive_id'=>$pipedrive_deal_id,
                                
                            ]);
        $quote->save();

        $seo_staff_cost = 78;
        $creative_staff_cost = 78;
        $dev_staff_cost = 78;
        $pm_staff_cost = 78; 

        // little tracking of what was actually saved. 
        $phases_saved = 0; 
        $phase_contingency = 0; 

        for($i=0; $i<count($phase_names); $i++) 
        {
            if($phase_names[$i]==="") continue;

            

            $phase = new QuotePhase([
                            'quote_id'=>$quote->id,
                            'phase_name'=>$phase_names[$i],
                            'phase_order'=>$i,
                            'start_date'=>$phase_start_date[$i],
                            'end_date'=>$phase_end_date[$i],
                            ]); 

            $phase->save();

            


            $seo_line = $phase_hours_seo[$i];
            $creative_line = $phase_hours_creative[$i];
            $development_line = $phase_hours_development[$i];
            $pm_line = $phase_hours_pm[$i];

            // Saves the digital marketing line item for this phase 
            if($seo_line>0) 
            {
                //
                $phase_contingency += ($phase_hours_seo[$i] * 0.2); 

                // TODO: Get the avg cost rate of the dept (by hour)
                $staff_cost = 78.00;

                // generate the line itemn for the qphase of the quote 
                $phase_line = new QuotePhaseLine([
                                                'phase_id'=>$phase->id,
                                                'dept_name'=>"Digital Marketing",
                                                'note'=>$phase_description_seo[$i],
                                                'hours'=>$phase_hours_seo[$i],
                                                'rate'=>165, // hard coded
                                                'staff_cost'=>$staff_cost,

                                            ]);
                $phase_line->save();

            }

            // Saves the creative line item for this phase 
            if($creative_line>0) 
            {

                $phase_contingency += ($phase_hours_creative[$i] * 0.2); 

                // TODO: Get the avg cost rate of the dept (by hour)
                $staff_cost = $creative_staff_cost;

                // generate the line itemn for the qphase of the quote 
                $phase_line = new QuotePhaseLine([
                                                'phase_id'=>$phase->id,
                                                'dept_name'=>"Creative",
                                                'note'=>$phase_description_creative[$i],
                                                'hours'=>$phase_hours_creative[$i],
                                                'rate'=>165, // hard coded
                                                'staff_cost'=>$staff_cost,

                                            ]);
                $phase_line->save();
            }

            // Saves the dev line item for this phase 
            if($development_line>0) 
            {
                // 
                $phase_contingency += ($phase_hours_development[$i] * 0.2); 

                // TODO: Get the avg cost rate of the dept (by hour)
                $staff_cost = $dev_staff_cost;

                // generate the line itemn for the qphase of the quote 
                $phase_line = new QuotePhaseLine([
                                                'phase_id'=>$phase->id,
                                                'dept_name'=>"Development",
                                                'note'=>$phase_description_development[$i],
                                                'hours'=>$phase_hours_development[$i],
                                                'rate'=>165, // hard coded
                                                'staff_cost'=>$staff_cost,

                                            ]);
                $phase_line->save();
            }

             // Saves the pm line item for this phase 
            if($pm_line>0) 
            {

                $phase_contingency += ($phase_hours_pm[$i] * 0.2); 

                // TODO: Get the avg cost rate of the dept (by hour)
                $staff_cost = $pm_staff_cost;

                // generate the line itemn for the qphase of the quote 
                $phase_line = new QuotePhaseLine([
                                                'phase_id'=>$phase->id,
                                                'dept_name'=>"PM",
                                                'note'=>$phase_description_pm[$i],
                                                'hours'=>$phase_hours_pm[$i],
                                                'rate'=>165, // hard coded
                                                'staff_cost'=>$staff_cost,

                                            ]);
                $phase_line->save();
            }

            // now add in the contingency line as a summary of everything that came before. 
            if($phase_contingency > 0 )
            {
                $phase_line = new QuotePhaseLine([
                                                'phase_id'=>$phase->id,
                                                'dept_name'=>"Contingency",
                                                'note'=>$phase_contingency_description[$i],
                                                'hours'=>$phase_contingency,
                                                'rate'=>165, // hard coded
                                                'staff_cost'=>$staff_cost, // TODO 

                                            ]);
                 $phase_line->save();

            }
            



            $phases_saved++; 


        }


//        return $phase_line;



        
       return redirect()->route('listQuotes'); 


       
    }   


    private function getPipedriveDeals()
    {
        // discovery stage = 4
        $call = pipedriveAPICall(4);
        
        if($call === FALSE)
        {
            return "Pipedrive API Error";
            exit; 
        }

        $all_deals = json_decode($call, true);
       
        $deals = [];
        

        foreach($all_deals['data'] as $deal)
        {   
           
            $deals[$deal['id']] = $deal['title'];
           
        }

        // Proposal stage = 5
        $call = pipedriveAPICall(5);

        $all_deals = json_decode($call, true);
       

       
        
        foreach($all_deals['data'] as $deal)
        {   
            $deals[$deal['id']] = $deal['title'];
        }

        return $deals; 


    }


}
