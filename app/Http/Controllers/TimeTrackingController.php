<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\User;
use App\TimeEntry; 
use App\Project;




class TimeTrackingController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function getHoursByEmployee($employee_id, $start=null,$end=null)
    {
           
        if(env('APP_ENV') == "local")
        {
            setlocale(LC_MONETARY, 'en_US');
        }else
        {   
            setlocale(LC_MONETARY, 'en_US.utf8');
        }

        if(is_null($start))
        {
            $week_of_the_year  = (int) date('W');
            $date = getTheWeek($week_of_the_year. true);
            $start =  $date->format('Y-m-d');
        }

        if(is_null($end))
        {
            $end = date("Y-m-d", strtotime($start. ' + 6 days'));
        }
        
       
        $entries = TimeEntry::where("employee_id", $employee_id)->whereBetween('date_entry', [$start, $end])->get();
        $staff = User::where("harvest_id",$employee_id)->first();

        // used for determining the actual bill rate 
        $project_rates = self::getProjectRates();
        $project_names = self::getProjectNames();
        $entries_by_project = [];
        $billable_hours_by_period = 0; 

        foreach ($entries as $entry) {
            
            if(!$entry->billable) continue;

            $billable_hours_by_period += $entry->entry;

            if(!array_key_exists($entry->project_id, $entries_by_project))
            {
                $o = new \stdClass(); 
                $o->project_name = $project_names[$entry->project_id];
                $o->rate = $project_rates[$entry->project_id];
                $o->hours = $entry->entry;
                $o->revenue = $o->rate * $entry->entry;
                $entries_by_project[$entry->project_id] = $o;
                continue; 
            }

            $o = $entries_by_project[$entry->project_id];
            $o->hours += $entry->entry;
            $o->revenue += ($entry->entry * $o->rate);

            $entries_by_project[$entry->project_id] = $o;


        }

        $sum_for_period = 0; 

        foreach ($entries_by_project as $project_id => $project) 
        {
           $sum_for_period += $project->revenue; 
        }


    	return view('auth.hours_by_employee', ["project_list"=>$entries_by_project, 
                                                "staff"=>$staff->name, 
                                                "start"=>$start, 
                                                "end"=>$end, 
                                                "sum"=>$sum_for_period]);
    	

    }

    // thiis ode is iidentical to the stuff found in the Harvet Controller 


    private static function getProjectRates()
    {
        // get an index of projects and their bill rates 
        $projects = Project::all();
        $project_rates = [];
        
        // transform the projects into an index for fast bill rate lookup 
        foreach ($projects as $project) {
            $project_rates[$project->project_id] = $project->effective_rate;
        }

        return $project_rates;

    }



    private static function getProjectNames()
    {
        // get an index of projects and their bill rates 
        $projects = Project::all();
        $project_names = [];
        
        // transform the projects into an index for fast bill rate lookup 
        foreach ($projects as $project) {
            $project_names[$project->project_id] = $project->project_name;
        }

        return $project_names;

    }

    
}
