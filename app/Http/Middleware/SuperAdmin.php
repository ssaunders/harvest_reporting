<?php

namespace App\Http\Middleware;


use Illuminate\Support\Facades\Auth;
use Closure;

/*
    this is a fairly hacky way to handle the higher level permissions shit that this application needs. 
    Hold your architectural critique 
*/

class SuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( !Auth::check() || !Auth::user()->isSuperAdmin() )
        {
           $request->session()->flash('error', 'You do not have permission to view this page');
           return redirect()->guest('/dashboard');
        }

        return $next($request);
    }
}
