<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',"Auth\LoginController@showLoginForm");


Auth::routes();

// user routes that require authentication
Route::group(['middleware' => ['auth']], function () {

	Route::get('/dashboard', 'DashboardController@revenueComparisonToCost')->name('dashboard'); 
	
	
	Route::get('/invoices', 'InvoiceController@getInvoiceAmounts')->name("invoices");
	Route::get('/invoice_list', 'InvoiceController@showInvoices')->name("invoicelist");
	Route::get('/invoicereport/{from}/{to}/{status?}', 'InvoiceController@getInvoicesByDate')->name("invoicereport");	
	Route::get('/revenue/{week_num?}', 'HarvestController@revenueByWeekNumber')->name("revenue");
	
	Route::get('/projects/{type?}', 'HarvestController@projectPageView')->name("projects");
	Route::get('/projects/details/{project_id}', 'ProjectController@showProjectDetails')->name("projectdetails");
	Route::get('/projects/details/{project_id}/addnote', 'ProjectController@addNoteView')->name("addNote");
	Route::post('/projects/details/{project_id}/addnote', 'ProjectController@postAddNote')->name("postAddNote");

	Route::get('/costs', 'HarvestController@generateWeeklyCostsAndRevenue')->name("deptcosts");
	Route::get('/notices', 'NoticesController@showNotices')->name("notices");
	Route::get('/time/{employee_id}/{from?}/{to?}', 'TimeTrackingController@getHoursByEmployee')->name('timesheets');
	Route::get('/invoices/compare/{month}/{year}/{compare_month}/{compare_year}/{service}', 'InvoiceController@compareMonthsByService')->name("compareInvoicesByMonth");

	Route::get('/expenses/project/{projectid}', 'ExpenseController@expensesByProject')->name("showexpenses");
	Route::get('/expenses/client/{clientid}', 'ExpenseController@expensesByClient')->name("showexpensesbyclient");

	Route::get('/quote', 'QuoteController@blankQuote')->name('newquote');
	Route::post('/quote', 'QuoteController@saveQuoteDetails')->name('saveQuoteDetails');
	Route::get('/quote/list', 'QuoteController@listQuotes')->name('listQuotes');
	Route::get('/quote/detail/{id}', 'QuoteController@details')->name('showQuoteDetail');
	Route::get('/quote/detail/{id}/edit', 'QuoteController@edit')->name('editQuoteDetail');
	
	Route::post('/quote/detail/{id}/update', 'QuoteController@updateQuoteDetails')->name('updateQuoteDetails');

	

});

Route::group(['middleware' => ['auth','admin']], function () {
	Route::get('/staff', "StaffController@showAll")->name("allstaff");
	Route::get('/clients', 'ClientController@showClients')->name("showclients");

	Route::get('/cashbalance', 'CashBalanceController@index')->name('cashbalance');
	Route::get('/cashbalance/add', 'CashBalanceController@create')->name('createcashbalance');
	Route::post('/cashbalance/add', 'CashBalanceController@store')->name('postcashbalance');
	Route::get('/cashbalance/list', 'CashBalanceController@listBalances')->name('listbalances');

	Route::get('/expenses/', 'ExpenseController@showExpenses')->name('listexpenses');

	Route::get('/expenses/add', 'ExpenseController@createOtherExpense')->name('createotherexpense');
	Route::get('/expenses/edit/{expense_id}', 'ExpenseController@editOtherExpense')->name('editotherexpense');
	Route::post('/expenses/add', 'ExpenseController@postOtherExpense')->name('postotherexpense');

	Route::get('/expenses/channel', 'ExpenseController@createExpenseChannel')->name('createexpensechannel');
	Route::post('/expenses/channel', 'ExpenseController@postBusinessExpenseChannel')->name('postexpensechannel');
	

	

	Route::get('/email/test', 'ReportController@sendTest')->name('emailtest');
	Route::get('email/weeklyreport', 'ReportController@sendWeeklyUtilizationReport')->name('weeklyemailreport');


});



Route::get('/home', 'HomeController@index')->name('home');
