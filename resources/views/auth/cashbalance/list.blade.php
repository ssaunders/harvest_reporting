@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            
                <h1>Account Balances</h1>
                <div class="panel panel-default">
                <div class="panel-body nopadding">
                    <div class="row">
                        <div class="text-right col-md-12"><a href="{{ route('createcashbalance')}}" class="btn">Update</a></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">

                            <div class="table-responsive table-hover">
                            <table class="table table-hover table-bordered" id='stickyHeader'>
                              <thead >
                                <tr>  
                                    
                                    <th>Account</th>
                                    <th>Balance</th>
                                    
                                </tr>
                                </thead>
                                <tfoot>

                                    <tr>
                                        <td>Total</td>
                                        <td class="text-right "><strong>{{  money_format('%1n',$total_cash) }}</strong></td>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach($accounts as $account_name=>$balance)
                                    <tr>
                                        <td>{{ ucwords($account_name) }}</td>
                                        <td class="text-right ">{{ money_format('%1n',$balance) }}</td>                      
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </div>
</div>
@endsection
