@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-body bigmetric positive_balance">
                     {{ money_format('%1n', $accounts_receivable) }}
                </div>
                <div class="panel-footer text-center">Total A/R (including past due)</div>
            </div>
        </div>

       

         <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-body bigmetric negative_balance">
                     {{ money_format('%1n', $late_invoices->sum('total_amount') - $late_invoices->sum('tax')) }}
                </div>
                <div class="panel-footer text-center">Past Due</div>
            </div>
        </div>

         <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-body bigmetric">
                     {{ money_format('%1n',$total_cash) }}
                </div>
                <div class="panel-footer text-center"><a href="{{ route('listbalances') }}">Cash on hand</a> (operating + warchest)</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responive">
                 <table class="table table-striped table-bordered lightbg" >
                     <tr>
                        <th></th>
                        @foreach($cashflow as $week=>$data)
    
                                <th>{{$week}}</th>
                        @endforeach
                     </tr>
                     <tr>
                        <th>Expected Revenue</th> 
                         @foreach($cashflow as $week=>$data)
    
                                <td>{{ money_format('%1n', $data->gross_payments) }}</td>
                        @endforeach
                     </tr>
                     <tr>
                        <th>Expenses</th> 
                         @foreach($cashflow as $week=>$data)
                                <td>{{ money_format('%1n', $data->expenses) }}</td>
                        @endforeach
                     </tr>
                     <tr>
                        <th>Balance</th> 
                         @foreach($cashflow as $week=>$data)
                                <td @if($data->balance < 0 ) class="negative_balance" @else class="positive_balance" @endif >{{ money_format('%1n', $data->balance) }}</td>
                        @endforeach
                     </tr>
                 </table>
            </div>
        </div>
    </div>
</div>
@endsection
