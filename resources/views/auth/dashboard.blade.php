@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
        
        <div class="chart-container">
            <canvas id="myChart" width="800" height="400"></canvas>
        </div>

        <div class="table-responsive table-hover">
            <table class="table table-hover table-bordered">
                      <thead>
                        <tr>  
                            <th>Month</th>
                            <th>Invoiced</th>
                            <th>Costs</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($costs_by_month as $month=>$amount)
                            <tr>
                                <td>{{ $month }}</td>
                                <td>{{ money_format('%1n', $invoiced_revenue_by_month[$month]) }}</td>
                                <td>{{ money_format('%1n', $amount) }}</td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var month_labels = {!! json_encode(array_keys($costs_by_month)) !!};
    var costs_data = {!! json_encode(array_values($costs_by_month))!!};
    var revenue_data = {!! json_encode(array_values($invoiced_revenue_by_month)) !!};
</script>




<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script type="text/javascript">

var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: month_labels,
        datasets: [{
            label: "Revenue",
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: revenue_data,
            fill:false
        },
        {
            label: "Costs",
            backgroundColor: 'rgb(0, 99, 132)',
            borderColor: 'rgb(0, 99, 244)',
            data: costs_data,
            fill:false
        }]
    },

    // Configuration options go here
    options: {animation: false}
});


</script>

@endsection