@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            
                <h1>Expenses for {{$projectname}}</h1>
                <div class="panel panel-default">
                <div class="panel-body nopadding">
                    <div class="table-responsive table-hover">
                    <table class="table table-hover table-bordered" id="stickyHeader">
                      <thead >
                        <tr>  
                            
                            <th>Harvest ID</th>
                            <th>Expense Category</th>
                            <th>Date Spent</th>
                            <th>Notes</th>
                            <th>Amount</th>
                            <th>Staff</th>
                            <th>Billed</th>
                            
                        </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <td>Total</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{ money_format('%1n', $expenses->sum('total_cost')) }}</td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($expenses as $expense)
                            <tr>
                                <td>{{ $expense->harvest_expense_id }}</td>
                                <td>{{ $expense->expense_category }}</td>
                                <td>{{ $expense->spent_date }}</td>
                                <td>{{ $expense->notes }}</td>                               
                                <td>{{ money_format('%1n',$expense->total_cost) }}</td>
                                <td>@if($expense->staff) {{ $expense->staff->name }} @endif</td>
                                <td>{{ $expense->is_billed }}</td>                            
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
                </div>    
            </div>
        </div>
    </div>
</div>
@endsection
