@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
            
           <div class="col-md-12"> <h1>Project:{{ $project->project_name }} </h1></div>

    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body bigmetric positive_balance">
                     {{ money_format('%1n', $project->project_budget) }}
                </div>
                <div class="panel-footer text-center">Budget</div>
            </div>
        </div>

       

         <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body bigmetric">
                     {{ $project->project_hours }}
                </div>
                <div class="panel-footer text-center">Hours to Date</div>
            </div>
        </div>

         <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body bigmetric">
                     {{ money_format('%1n',$project->effective_rate) }}
                </div>
                <div class="panel-footer text-center">Effective Rate</div>
            </div>
        </div>
         <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body bigmetric">
                     {{ money_format('%1n',$project->expenses->sum('total_cost')) }}
                </div>
                <div class="panel-footer text-center"><a href="{{ route('showexpenses', ['project_id'=>$project->project_id]) }}">Project Expenses</a></div>
            </div>
        </div>
    </div>

    <div class="row">
            
            <div class="col-md-12"><h2>Invoices </h2></div>

    </div>

    <div class="row">
         <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body bigmetric ">
                     {{  money_format('%1n', $invoiced['Services'] ) }}
                </div>
                <div class="panel-footer text-center">Service Fees</div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body bigmetric ">
                     {{  money_format('%1n', $invoiced['Digital Marketing Services'] )}}
                </div>
                <div class="panel-footer text-center">Digital Marketing Fees</div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body bigmetric ">
                     {{   money_format('%1n', $invoiced['Expenses'] ) }}
                </div>
                <div class="panel-footer text-center">Expenses</div>
            </div>
        </div>

         <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body bigmetric ">
                     {{  money_format('%1n', $invoiced['Media'] ) }}
                </div>
                <div class="panel-footer text-center">Media</div>
            </div>
        </div>

    </div>
    <hr>

    <div class="row">
        <div class="col-md-6"><h2>Notes</h2></div>
        <div class="col-md-6 text-right"><a href="{{ route('addNote', ['project_id'=>$project->project_id]) }}" class="btn btn-default">Add Note</a></div>
    </div>
    <div class="row">
        <div class="col-md-12">
            
            @if(count($project->notes) == 0 )
                You have no notes.  
            @else
                @foreach($project->notes as $note)
                <div class="panel panel-default">
                <div class="panel-heading"><small><strong>{{ $note->note_type }}</strong></small></div>  
                <div class="panel-body">
                     {!! nl2br($note->content) !!}
                </div>
                </div>
                @endforeach
            @endif
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <canvas id="myChart" width="800" height="400"></canvas>
        </div>
    </div>
</div>

<script type="text/javascript">
    var month_labels = {!! json_encode(array_keys($time_months_billable)) !!};
    var billable_time = {!! json_encode(array_values($time_months_billable)) !!};
    var non_billable_time = {!! json_encode(array_values($time_months_nonbillable)) !!};
</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script type="text/javascript">

var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: month_labels,
        datasets: [{
            label: "Billable Time",
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: billable_time,
            fill:false
        },
        {
            label: "Non-Billable Time",
            backgroundColor: 'rgb(0, 99, 132)',
            borderColor: 'rgb(0, 99, 244)',
            data: non_billable_time,
            fill:false
        }]
    },

    // Configuration options go here
    options: {animation: false}
});


</script>
@endsection
