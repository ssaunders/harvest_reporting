@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
        <div class="table-responsive">
            @foreach($notices as $notice)
            <table class="table table-striped table-bordered">
                      <thead>
                        <tr>  
                            <th>{{$notice->name}}</th>
                        </tr>
                        </thead>
                         <tfoot>
                            <tr>
                              <td>{{ $notice->created_at->diffForHumans() }}</td>
                            </tr>
                          </tfoot>
                        <tbody>
                            <tr>
                                <td>{{ $notice->message }}</td>
                            </tr>
                        </tbody>
                    </table>
            @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
