@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
        <div class="table-responsive table-hover">
            <table class="table table-hover table-bordered">
                      <thead>
                        <tr>  
                            <th>Name</th>
                            <th>Harvest ID</th>
                            <th>Salary</th>
                            <th>Extras</th>
                            <th>Effective Date</th>
                            <th>Salary per Hour</th>
                            <th>Cost per Hour</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($staff as $user)
                            <tr>
                                <td><a href="{{ route('timesheets', $user->harvest_id) }}">{{ $user->name }}</td>
                                <td>{{ $user->harvest_id }} </td>
                                <td>{{ money_format('%1n',$user->salary) }}</td>
                                <td>{{ money_format('%1n',$user->agency_extras) }} </td>
                                <td>{{ $user->effective_date }}</td>
                                <td>{{ money_format('%1n',$user->base_hourly_cost_rate) }}</td>
                                <td>{{ money_format('%1n',$user->hourly_cost_rate) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>
@endsection
