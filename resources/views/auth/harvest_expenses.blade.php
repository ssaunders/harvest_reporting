@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            
                <h1>Expenses for {{$projectname}}</h1>
                <div class="panel panel-default">
                <div class="panel-body nopadding">
                    <div class="table-responsive table-hover">
                    <table class="table table-hover table-bordered" id="stickyHeader">
                      <thead >
                        <tr>  
                            
                            <th>Harvest ID</th>
                            <th>Project</th>
                            <th>Expense Category</th>
                            <th>Date Spent</th>
                            <th>Notes</th>
                            <th>Amount</th>
                            <th>Staff</th>
                            <th>Billed</th>
                            
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($expenses as $expense)
                            <tr>
                                <td>{{ $expense->harvest_expense_id }}</td>
                                <td>{{ $expense->project->project_name }}</td>
                                <td>{{ $expense->expense_category }}</td>
                                <td>{{ $expense->spent_date }}</td>
                                <td>{{ $expense->notes }}</td>                               
                                <td>{{ money_format('%1n',$expense->total_cost) }}</td>
                                <td>{{ $expense->staff->name }}</td>
                                <td>{{ $expense->is_billed }}</td>                            
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
                </div>    
            </div>
        </div>
    </div>
</div>
@endsection
