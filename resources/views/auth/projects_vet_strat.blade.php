@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            
                <h1>Projects By Type: {{ ucwords($type) }}</h1>
                <div class="panel panel-default">
                <div class="panel-body nopadding">
                    <div class="table-responsive table-hover">
                    <table class="table table-hover table-bordered" id="stickyHeader">
                      <thead >
                        <tr>  
                            <th>Project</th>
                            <th>Client</th>
                            <th>Budget</th>
                            <th>Invoices</th>
                            <th>Left to Bill</th>
                            <th>Hours</th>
                            <th>Hourly Rate</th>
                            <th>Effective Rate</th>
                            <th>Last Time Entry</th>
                            
                            <th>Rate by Invoice</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($project_list as $project)
                            <tr>
                                <td>{{ $project->project_name }} {{ $project->project_id }}</td>
                                <td>{{ $project->client_name }} </td>                               
                                <td>{{ money_format('%1n',$project->project_budget) }} </td>
                                <td>{{ money_format('%1n', $project->billed) }}</td>
                                <td>{{ money_format('%1n',$project->left_to_bill) }} </td>
                                <td>{{ $project->project_hours }} </td>
                                <td>{{ $project->hourly_rate }}</td>
                                <td>{{ $project->effective_rate }}</td>
                                <td>@if(!empty($project->last_time_entry) ) {{ $project->last_time_entry->diffForHumans() }}@else --- @endif </td>
                                <td>{{ money_format('%1n', $project->invoice_hours) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
                </div>    
            </div>
        </div>
    </div>
</div>
@endsection
