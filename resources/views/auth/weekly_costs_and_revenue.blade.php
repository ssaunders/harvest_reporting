@extends('layouts.app')

@section('content')

<div class="container-fluid">
 @if(Auth::user()->isAdmin())    
<div class="row">
  <div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-body bigmetric @if($ytd_pl['PM']->revenue < $ytd_pl['PM']->costs) negative_balance @else positive_balance @endif">
             {{ money_format('%1n', ($ytd_pl['PM']->revenue - $ytd_pl['PM']->costs)) }}
        </div>
        <div class="panel-footer text-center">Project Management P/L</div>
    </div>

  </div>

  <div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-body bigmetric @if($ytd_pl['Creative']->revenue < $ytd_pl['Creative']->costs) negative_balance @else positive_balance @endif">
             {{ money_format('%1n', ($ytd_pl['Creative']->revenue - $ytd_pl['Creative']->costs)) }}
        </div>
        <div class="panel-footer text-center">Creative P/L</div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-body bigmetric @if($ytd_pl['Development']->revenue < $ytd_pl['Development']->costs) negative_balance @else positive_balance @endif">
             {{ money_format('%1n', ($ytd_pl['Development']->revenue - $ytd_pl['Development']->costs)) }}
        </div>
        <div class="panel-footer text-center">Development P/L</div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-body bigmetric @if($ytd_pl['Marketing']->revenue < $ytd_pl['Marketing']->costs) negative_balance @else positive_balance @endif">
             {{ money_format('%1n', ($ytd_pl['Marketing']->revenue - $ytd_pl['Marketing']->costs)) }}
        </div>
        <div class="panel-footer text-center">Marketing P/L</div>
    </div>
  </div>

</div>
@endif

            @foreach($all_weeks as $month=>$weeks)


            <div class="row">
              <div class="col-md-12">
              <h2>{{ $month }}</h2>
              </div>
            </div>
            @foreach($weeks as $week_label=>$week)
            <div class="row">
            <div class="col-md-12">
            <div class="table-responsive table-hover">
            <h3>{{ $week_label }}</h3>
            <table class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th class="col-md-1">Dept</th>
                          <th class="col-md-2" align="right">Cost</th>
                          <th class="col-md-2" align="righ">Revenue Earned</th>
                          <th class="col-md-1" align="right">Target Hours</th>
                          <th class="col-md-2" align="right">Revenue Hours</th>
                          <th class="col-md-2">Avg Effective Rate</th>
                          <th class="col-md-2">Percent</th>
                      </tr>
                     </thead>
                        <tbody>
                            
                        @foreach($week['costs_and_revenues'] as $key=>$val)
                            <tr id="wrap">
                               
                                <td><a  
                                  role="button"
                                  data-toggle="collapse" 
                                  data-parent="#wrap" 
                                  data-target="#employeeCosts_{{$key}}_{{$week_label}},#employeeRevenues_{{$key}}_{{$week_label}},#employeeHours_{{$key}}_{{$week_label}}"  
                                  aria-expanded="false" 
                                  aria-controls="#employeeCosts_{{$key}}_{{$week_label}},#employeeRevenues_{{$key}}_{{$week_label}},#employeeHours_{{$key}}_{{$week_label}}">{{ $key }}</a>
                                    
                                </td>
                                <td align="right"> {{ money_format('%1n', $val[0]) }} 
                                  
                                  @if(Auth::user()->isAdmin())
                                      @if(!empty($week['costs_by_employee'][$key]))
                                      <div class="collapse" id="employeeCosts_{{$key}}_{{$week_label}}">
                                          <table class="table table-striped table-bordered table-condensed">
                                            @foreach($week['costs_by_employee'][$key] as $staff=>$cost)
                                                <tr><td>{{$staff}}</td><td align="right">{{money_format('%1n', $cost)}}</td></tr>
                                            @endforeach
                                          </table>
                                      </div>
                                      @endif
                              
                                  @endif

                                 

                                </td>
                                <td align="right"> {{ money_format('%1n', $val[1]) }} 

                                  @if(!empty($week['revenue_by_employee'][$key]))
                                      <div class="collapse" id="employeeRevenues_{{$key}}_{{$week_label}}">
                                          <table class="table table-striped table-bordered table-condensed">
                                            @foreach($week['revenue_by_employee'][$key] as $staff=>$revenue)
                                                <tr><td>{{$staff}}</td><td align="right">{{money_format('%1n', $revenue)}}</td></tr>
                                            @endforeach
                                          </table>
                                      </div>
                                  @endif
                                </td>
                                <td> @if(!empty($val[2])) {{ $val[2] }} @endif</td>
                                <td align="right"> @if(!empty($val[3])) {{ $val[3] }} @endif
                                  
                                   @if(!empty($week['hours_by_dept_by_employee'][$key]))
                                      <div class="collapse" id="employeeHours_{{$key}}_{{$week_label}}">
                                          <table class="table table-striped table-bordered table-condensed">
                                            @foreach($week['hours_by_dept_by_employee'][$key] as $staff=>$hours)
                                                <tr><td><a href="{{route('timesheets', ['employee_id'=>$staff_ids[$staff], 'from'=>$week_label])}}">{{$staff}}</a></td><td align="right">{{$hours}}</td></tr>
                                            @endforeach
                                          </table>
                                      </div>
                                  @endif

                                </td>
                                <td> @if(!empty($val[3]) && !empty($val[2])) {{ money_format('%1n', ($val[1]/$val[3]))  }} @endif</td>
                               
                                @if($loop->iteration == 1)
                                <td 
                                rowspan="{{ count($week['costs_and_revenues']) + 2 }}" 
                                style="vertical-align:middle; font-size:3em; text-align:center;"
                                @if ($week['percent_to_target'] > 99) class="success"  @endif
                                @if ($week['percent_to_target'] >= 90 && $week['percent_to_target'] < 100) class="warning" @endif
                                @if ($week['percent_to_target'] < 90)  class="danger" @endif >  {{ $week['percent_to_target'] }}%</td>
                                @endif
                            </tr>
                          
                         @endforeach  
                         <tr>
                                <td>Totals</td>
                                <td align="right">{{ money_format('%1n', $week['total_cost']) }} </td>
                                <td align="right">{{ money_format('%1n', $week['total_revenue']) }} </td>
                                <td>...</td>
                                <td align="right">{{  $week['total_hours'] }} </td>
                                <td>...</td>
                         </tr>

                         
                        
                        </tbody>
            </table>
            </div>
        </div>
    </div>
            @endforeach
           
            @endforeach
            
</div>
@endsection
