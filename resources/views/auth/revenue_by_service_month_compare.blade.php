@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            
                <h1>Revenue Comparison: {{ $month }} - {{$compare}}</h1>
                <h2>{{ $service }}</h2>
                <div class="panel panel-default">
                <div class="panel-body nopadding">
                    <div class="table-responsive table-hover">
                    <table class="table table-hover table-bordered" id="stickyHeader">
                      <thead >
                        <tr>  
                            <th class="col-md-3">Client</th>
                            <th class="col-md-3">{{ $month }}</th>
                            <th class="col-md-3">{{ $compare }}</th>
                            <th class="col-md-3">Difference</th>
                           
                        </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <td></td>
                                <td>{{ money_format('%1n',$firstmonthsum) }}</td>
                                <td>{{ money_format('%1n',$secondmonthsum) }}</td>
                                <td>{{ money_format('%1n',($secondmonthsum - $firstmonthsum)) }}</td>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($clients as $client=>$amount_array)
                            <tr>
                                <td>{{ $client}} </td>
                                <td>{{ money_format('%1n', $amount_array[0]) }} </td>
                                <td>{{ money_format('%1n', $amount_array[1]) }}</td>
                                <td>{{ money_format('%1n',($amount_array[1] - $amount_array[0])) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


            </div>
                </div>    
            </div>
        </div>
    </div>
</div>
@endsection
