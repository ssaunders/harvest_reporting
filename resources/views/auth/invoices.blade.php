@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
        <div class="table-responsive table-hover">
            <table class="table table-hover table-bordered" id="stickyHeader">
                      <thead>
                        <tr>  
                            <th>Invoice number</th>
                            <th>Date</th>
                            <th>Client</th>
                            <th>Amount</th>
                            <th>Tax</th>
                            <th>Lines Attributed</th>
                            <th>Lines Unattributed</th>
                            <th>Status</th>
                
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($invoices as $invoice)
                            <tr>
                                <td><a href="https://artandscience.harvestapp.com/invoices/{{ $invoice->harvest_id }}" target="_blank">{{ $invoice->invoice_number }}</a></td>
                                <td>{{ $invoice->issue_date }} </td>
                                <td>{{ $invoice->client_id }} </td>
                                <td>{{ money_format('%1n',$invoice->total_amount) }} </td>
                                <td>{{ money_format('%1n',$invoice->tax) }} </td>
                                <td>{{ count($invoice->lines->where("project_id", "!=", NULL)) }}</td>
                                <td>{{ count($invoice->lines->where("project_id", NULL)) }} </td>
                                <td>{{ $invoice->status }} </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>
@endsection
