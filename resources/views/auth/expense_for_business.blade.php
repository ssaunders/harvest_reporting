@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
                
                 <a href="{{ route('createotherexpense') }}" class="btn btn-default pull-right"  role="button">Add Expense</a>

                <h1>Expenses</h1>

                <div class="panel panel-default">
                  

                <div class="panel-body nopadding">

                    <div class="table-responsive table-hover">
                    <table class="table table-hover table-bordered" id="stickyHeader">
                      <thead >
                        <tr>  
                            
                            <th>Expense Category</th>
                            <th>Due Date</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th></th>
                            
                        </tr>
                        </thead>
                       
                        <tbody>
                            @foreach($expenses as $expense)
                            <tr>
                               
                                <td>{{ $expense->channel->channel }}</td>
                               
                                <td>{{ $expense->due_date }}</td>       
                                                              
                                <td>{{ money_format('%1n',$expense->amount) }}</td>
                                
                                <td>{{ $expense->status }}</td> 

                                <td> <a href='{{ route("editotherexpense", ["expense_id"=>$expense->id]) }}'>EDIT</a></td>                           
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
                </div>    
            </div>
        </div>
    </div>
</div>
@endsection
