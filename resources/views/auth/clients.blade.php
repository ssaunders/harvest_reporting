@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            
                <h1>Clients</h1>
                <div class="panel panel-default">
                <div class="panel-body nopadding">
                    <div class="table-responsive table-hover">
                    <table class="table table-hover table-bordered" id="stickyHeader">
                      <thead >
                        <tr>  
                            
                            <th>Client</th>
                            <th>Project Count</th>
                            <th>Invoices Issued</th>
                            <th>Revenue</th>
                            <th>Hours</th>
                            <th>Services Revenue</th>
                            <th>Effective Rate</th> 
                            <th>Rate by Invoice</th>
                            <th>Expenses</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($clients as $client)
                            <tr>
                                <td>{{ $client->client_name }}</td>
                                <td>{{ $client->projects_count }}</td>                               
                                <td>{{ $client->issued_invoices_count }}</td>
                                <td>{{ money_format('%1n',$client->issuedInvoices()->sum('total_amount')) }}</td>
                                <td>{{ $client->projects()->sum('project_hours') }}</td> 
                                <td>{{ money_format('%1n',$service_revenue[$client->client_id]) }}</td> 
                               
                                 <td>
                                    @if($client->projects()->sum('project_hours') > 0)
                                        {{ money_format('%1n',($service_revenue[$client->client_id]/$client->projects()->sum('project_hours'))) }}
                                    @else
                                        --
                                    @endif
                                </td> 
                                <td></td>
                                <td><a href="{{ route('showexpensesbyclient', ['clientid'=>$client->client_id]) }}">{{ money_format('%1n',$expenses[$client->client_id]) }}</a></td>
                                
                            
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
                </div>    
            </div>
        </div>
    </div>
</div>
@endsection
