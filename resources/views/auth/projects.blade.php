@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            
                <h1>Projects By Type: {{ ucwords($type) }}</h1>

                <input type="checkbox" name="showarchives" id="archive_control" value="true" @if($showarchives == "true") checked="checked" @endif> Show Archived Projects
                
                <div class="panel panel-default">
                <div class="panel-body nopadding">
                    <div class="table-responsive table-hover">
                    <table class="table table-hover table-bordered" id="stickyHeader">
                      <thead >
                        <tr>  
                            <th>Project</th>

                            <th>Client</th>
                            <th>Budget</th>
                            <th>Invoices</th>
                            <th>Left to Bill</th>
                            <th>Hours</th>
                            <th>Hourly Rate</th>
                            <th>Effective Rate</th>
                            <th>Last Time Entry</th>
                            <th>Rate by Invoice</th>
                            <th>Expenses</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($project_list as $project)

                            <tr>
                                <td><a href="{{ route('projectdetails', ['project_id'=>$project->project_id])  }}">{{ $project->project_name }}</a>
                                    @if(!$project->is_active)
                                        <span class="label label-danger">Closed</span>
                                    @endif
                                </td>
                                <td>{{ $project->client_name }} </td>                               
                                <td>{{ money_format('%1n',$project->project_budget) }} </td>
                                <td>{{ money_format('%1n', $project->billed) }}</td>
                                <td>{{ money_format('%1n',$project->left_to_bill) }} </td>
                                <td>{{ $project->project_hours }} </td>
                                <td>{{ $project->hourly_rate }}</td>
                                <td>{{ $project->effective_rate }}</td>
                                <td>@if(!empty($project->last_time_entry) ) {{ $project->last_time_entry->diffForHumans() }}@else --- @endif </td>
                                <td>{{ money_format('%1n', $project->invoice_hours) }}</td>
                                <td><a href="{{ route('showexpenses', ['projectid'=>$project->project_id]) }}">{{ money_format('%1n',$project->expenses->sum("total_cost")) }}</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
                </div>    
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    
    $("#archive_control").change(function(){

        console.log(this.checked);


        if(this.checked)
        {
            window.location.href = "?showarchives=true";           
        }else
        {
            window.location.href = "?showarchives=false";           

        }
 
    });

</script>

@endsection