@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
             <h1>Revenue By Invoices 2021</h1>
        <div class="panel panel-default">
        <div class="panel-body nopadding">
            <div class="table-responsive">
            <table class="table table-striped table-bordered" >
                      <thead>
                        <tr>
                          <th></th>
                            @foreach($months as $key=>$value)
                            <th>{{ $key }}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Media</td>
                                @foreach($months as $key=>$value)
                                    <td>{{ money_format('%1n',$value->media_total) }}</td>
                                @endforeach
                            </tr>
                            <tr>
                                <td>Expenses</td>
                                @foreach($months as $key=>$value)
                                    <td>{{ money_format('%1n',$value->expenses_total) }}</td>
                                @endforeach
                            </tr>
                            <tr>
                                <td>Services</td>
                                @foreach($months as $key=>$value)
                                <?php  
                                        
                                        $split = explode("_", $key);
                                       
                                        
                                        $date = $split[0]." 01 ".$split[1];
                                       

                                        $month_num = date('m',strtotime($date));
                                        $year_num = date('Y', strtotime($date));

                                        if($month_num - 1 == 0 ) 
                                        {
                                            $compare_month = 12; 
                                            $compare_year = 2019;
                                        }else
                                        {
                                            $compare_month = $month_num - 1;
                                            $compare_year = $year_num;
                                        }
                                ?>
                                    <td><a href="{{ route('compareInvoicesByMonth', [$month_num, $year_num, $compare_month, $compare_year, 'Services']) }}">
                                        {{ money_format('%1n',$value->services_total) }}
                                    </a></td>
                                @endforeach
                            </tr>
                            <tr>
                                <td>Digital Marketing</td>
                                @foreach($months as $key=>$value)
                                    <?php  
                                        $split = explode("_", $key);
                                       
                                        
                                        $date = $split[0]." 01 ".$split[1];
                                       

                                        $month_num = date('m',strtotime($date));
                                        $year_num = date('Y', strtotime($date));

                                        if($month_num - 1 == 0 ) 
                                        {
                                            $compare_month = 12; 
                                            $compare_year = 2019;
                                        }else
                                        {
                                            $compare_month = $month_num - 1;
                                            $compare_year = $year_num;
                                        }
                                      
                                     ?>
                                    <td><a href="{{ route('compareInvoicesByMonth', [$month_num, $year_num, $compare_month, $compare_year, 'Digital Marketing Services']) }}">
                                        {{ money_format('%1n',$value->digital_services_total) }}</a></td>
                                    
                                @endforeach
                            </tr>
                            <tr class="subtotal">
                                <td>Services Subtotal</td>
                                @foreach($months as $key=>$value)
                                    <td>{{ money_format('%1n',($value->digital_services_total+$value->services_total)) }}</td>
                                @endforeach
                            </tr>
                            <tr class="totals">
                                <td>Totals:</td>
                                @foreach($months as $key=>$value)
                                    <td><strong>{{ money_format('%1n',($value->services_total+$value->media_total+$value->expenses_total+$value->digital_services_total)) }}</strong></td>
                                @endforeach
                            </tr>
                           
                            <tr>
                                <td colspan="12">Target vs. Total</td>
                            </tr>

                            <tr  class="totals" >
                                <td>Rolling Target:</td>
                                @foreach($months as $key=>$value)
                                    <td><strong>{{ money_format('%1n',($value->rolling_target)) }}</strong></td>
                                @endforeach
                            </tr>
                            <tr  class="totals">
                                <td>Rolling Total:</td>
                                @foreach($months as $key=>$value)
                                    <td @if($value->rolling_total < $value->rolling_target) class="negative" @else class="positive" @endif><strong>{{ money_format('%1n',($value->rolling_total)) }}</strong></td>
                                @endforeach
                            </tr>
                            <tr  class="totals">
                                <td>Rolling Difference:</td>
                                @foreach($months as $key=>$value)
                                    <td @if($value->rolling_difference < 0) class="negative" @else class="positive" @endif><strong>{{ money_format('%1n',($value->rolling_difference)) }}</strong></td>
                                @endforeach
                            </tr>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
        </div>
    </div>
</div>
@endsection
