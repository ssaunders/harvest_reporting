@extends('layouts.app')

@section('content')
<div class="container-fluid">


    <div class="row">
        <div class="col-md-12 text-right">
            <a href="{{ route('newquote') }}" class="btn btn-default">New Quote</a>
        </div>
    </div>


    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-body bigmetric ">
                     {{ money_format('%1n', $total_active_quotes_low) }}
                </div>
                <div class="panel-footer text-center">All Quotes (low)</div>
            </div>
        </div>

       

         <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-body bigmetric ">
                     {{ money_format('%1n', $total_active_quotes_high) }}
                </div>
                <div class="panel-footer text-center">All Quotes (high)</div>
            </div>
        </div>

         <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-body bigmetric">
                    {{ $quote_count }}
                </div>
                <div class="panel-footer text-center">Total Quotes</div>
            </div>
        </div>
    </div>


    <div class="row">



        <div class="col-md-12">
        <div class="table-responsive table-hover">
            <table class="table table-hover table-bordered">
                      <thead>
                        <tr>  
                            <th>Name</th>
                            <th>Client/Prospect</th>
                            <th>Owner</th>
                            <th>Phases</th>
                            <th>Total Fee</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($quotes as $quote)
                            <tr>
                                <td><a href="{{ route('showQuoteDetail', [$quote->id]) }}">{{ $quote->description }}</td>
                                <td>--</td>
                                <td>{{ $quote->staff->name }}</td>
                                <td>{{ count($quote->phases) }} </td>
                                <td>{{ money_format('%1n',$quote->jobCostLow) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>
@endsection
