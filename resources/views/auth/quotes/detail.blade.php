@extends('layouts.app')

@section('content')
<div class="container">


    <div class="row">
        <h3>{{ $quote->description }}</h3>

        <div class="col-md-6">
           
          <div class="table-responsive table-hover">

                    <table class="table table-hover table-bordered" >

                @foreach($quote->phases as $phase)
                    <tr>
                        <td colspan="3">
                            <strong>{{ $phase->phase_name }}</strong>
                        </td>
                    </tr>

                    @foreach($phase->lines as $line)
                    <tr>
                        <td class="col-md-4">
                            {{  $line->dept_name }}
                        </td>
                        <td class="col-md-4">
                            {{  $line->hours }}
                        </td>
                        <td class="col-md-4">
                            {{  money_format('%1n', $line->hours * 165) }}
                        </td>
                    </tr>
                    @endforeach
                    <tr>
                        <td><strong>Phase Cost Low</strong></td>
                        <td></td>
                        <td><strong>{{ money_format('%1n',$phase->phaseCostsLow->first()->cost) }}</strong></td>
                    </tr>

                    <tr>
                        <td><strong>Phase Cost High</strong></td>
                        <td></td>
                        <td><strong>{{ money_format('%1n',$phase->phaseCostsHigh->first()->cost) }}</strong></td>
                    </tr>



                @endforeach   

                 <tr>
                    <td colspan="3"><strong>SUMMARY</strong></td>
                <tr> 


                <tr>
                    <td colspan="2"><strong>TOTAL JOB COST LOW</strong></td>
                  
                    <td><strong>{{ money_format('%1n', $quote->jobCostLow) }}</strong></td>
                <tr> 


                <tr>
                    <td colspan="2"><strong>TOTAL JOB COST HIGH</strong></td>
                    
                    <td><strong>{{ money_format('%1n',$quote->jobCostHigh) }}</strong></td>
                <tr> 

                </table>

                </div>     
        </div>
        <div class="col-md-3 ">
            <a href="{{ route('editQuoteDetail', ['id'=>$quote->id]) }}" class="btn btn-default">EDIT</a>
        </div>
        <div class="col-md-3 text-right">
            <a href="{{ route('listQuotes') }}" class="btn btn-default">BACK TO LIST</a>
        </div>
        

    </div>
</div>
@endsection

@section('scripts')



@endsection