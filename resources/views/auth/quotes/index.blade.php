@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h1>Generate a Quote</h1>
    </div>

    <div class="row">
       

            <div class="col-md-12">
                 <h3>Design / Dev Quote Template </h3>         
             <div class='row'>
                    
                        <div class="col-md-3">
                            <div class="panel panel-default text-center">
                                <div class="panel-body bigmetric">
                                     <div id="job_hours_total">0</div>
                                </div>
                                <div class="panel-footer text-center">Hours Total (low)</div>
                            </div> 
                        </div>

                        <div class="col-md-3">
                            <div class="panel panel-default text-center">
                                <div class="panel-body bigmetric">
                                     <div id="job_hours_total_high">0</div>
                                </div>
                                <div class="panel-footer text-center">Hours Total (high)</div>
                            </div> 
                        </div>
                        <div class="col-md-3">                                        
                            <div class="panel panel-default text-center">
                                <div class="panel-body bigmetric">
                                     <div id="job_cost_total">$0</div>
                                </div>
                                <div class="panel-footer text-center">Fee (low)</div>
                            </div> 
                        </div>

                        <div class="col-md-3">                                        
                            <div class="panel panel-default text-center">
                                <div class="panel-body bigmetric">
                                     <div id="job_cost_total_high">$0</div>
                                </div>
                                <div class="panel-footer text-center">Fee (high)</div>
                            </div> 
                        </div>

                </div>

        </div>

        <div class="col-md-6">
            <form id="formQuote" method="post" action="{{ route('saveQuoteDetails') }} ">
            @if($edit)
            <input type="hidden" name="pipedrive_deal_id" value="{{ $quote->pipedrive_id }}" />
            @endif
            <div class="form-group">
            <label><strong>Pick a Deal</strong></label>
                <select name="pipedrive_deal" class="form-control" id="pipedrive_deal">
                    <option value="" selected="selected">=== Select Pipedrive Deal ===</option>
                    @foreach($deals as $deal_id=>$deal_title)
                        <option value="{{ $deal_id }}">{{ $deal_title }}</option>
                    @endforeach

                </select>
            </div>

            
            <div id="templatePhase">
                <hr>
                <div class="form-group">
                    <label >Phase Name</label>
                    <input type="text" class="form-control phase_name" name="phase[]" placeholder="Name of the Phase">
                    <input type="hidden" class="phase_id" name="phase_id[]">
                </div>

                <div class="row">
                    
                    <div class="col-md-6">                
                        <div class="form-group">
                            <label>Start Date:</label>
                            <input type="date" class="form-control phase_start_date" name="phase_start_date[]" required="">
                        </div>
                    </div>
                    

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>End Date:</label>
                            <input type="date" class="form-control phase_end_date" name="phase_end_date[]">
                        </div>
                    </div>

                </div>



               
                

                <!--<div class="form-group" id="phase1">
                    <button type="button" id="btnShowDetails" class="btn btn-default "> Details</button>
                    
                 </div>
                -->
                 <div  id="details_phase" >
                    <div class="row">
                                       
                        
                        <div class="col-md-3" >
                            <div class="form-group">
                                <label>SEM / SEO Hours</label>
                                <input type="number" class="form-control hours hours_seo" name="hours_seo[]" placeholder="Hours">
                                <input type="hidden" class="line_id" name="line_id[]">
                            </div>
                        </div>

                        <div class="col-md-9" >
                            <div class="form-group">
                                <label>Description</label>
                                <input type="text" class="form-control description description_seo" name="description_seo[]" placeholder="describe efforts in this phase.">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                       
                        <div class="col-md-3" >
                            <div class="form-group">
                                <label>Creative Hours</label>
                                <input type="number" class="form-control hours hours_creative" name="hours_creative[]" placeholder="Hours">
                            </div>
                        </div>

                        <div class="col-md-9" >
                            <div class="form-group">
                                <label>Description</label>
                                <input type="text" class="form-control description description_creative" name="description_creative[]" placeholder="describe efforts in this phase.">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        

                        <div class="col-md-3" >
                            <div class="form-group">
                                <label>Dev Hours</label>
                                <input type="number" class="form-control hours hours_development" name="hours_development[]" placeholder="Hours">
                            </div>
                        </div>

                        <div class="col-md-9" >
                            <div class="form-group">
                                <label>Description</label>
                                <input type="text" class="form-control description description_development" name="description_development[]" placeholder="describe efforts in this phase.">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                      
                        <div class="col-md-3" >
                            <div class="form-group">
                                <label>PM Hours</label>
                                <input type="number" class="form-control hours hours_pm" name="hours_pm[]" placeholder="Hours">
                            </div>
                        </div>

                        <div class="col-md-9" >
                            <div class="form-group">
                                <label>Description</label>
                                <input type="text" class="form-control description description_pm" name="description_pm[]" placeholder="describe efforts in this phase.">
                            </div>
                        </div>


                    </div>

                    <div class="row">
                      
                        <div class="col-md-3" >
                            <div class="form-group">
                                <label>Contingency</label>
                                <input type="number" disabled="disabled" class="form-control contingency_hours hours_contingency" name="hours_contingency[]" placeholder="Hours">
                            </div>
                        </div>

                        <div class="col-md-9" >
                            <div class="form-group">
                                <label>Description</label>
                                <input type="text" class="form-control description description_contingency" name="description_contingency[]" placeholder="describe efforts in this phase.">
                            </div>
                        </div>


                    </div>

                <div class='row' style="padding-bottom:20px;">
                    
                        <div class="col-md-3"> 
                            <div class="form-group">
                                <label>Hours Total</label>
                                <input type="number" disabled="disabled" class="form-control phase_hours_total" value="0">
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group"> 
                                <label>Fee</label>
                                <input type="text" disabled="disabled" class="form-control phase_cost_total" value="$0.00">
                            </div>
                        </div>
                </div>
          
                </div>
            </div>





            <div class="row" id="add_phase_button_container">
                <div class="col-md-12" style="padding-bottom:20px;">
                <button type="button" id="btnAddPhase" class="btn btn-default btn-primary" style="width:100%;" aria-label="Left Align">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    Add Phase
                </button>   
                </div>
            </div>
            <div class="row" >
                <div class="col-md-12">
                    <input type="submit" id="submitbtn" class="btn btn-default" value="Save Quote" />
                </div>

            </div>
              
          </form>
        </div>


    </div>
</div>
@endsection

@section('scripts')

<script>

var phase_count = 1; 



$(".hours").change(function(){

    recalculateFees();

});




function recalculateFees()
{
    
    var contingency_hours = 0; 
    var job_hours_low = 0;
    var job_hours_high = 0;
    var job_low_estimate = 0; 
    var job_high_estimate = 0; 


    // find each section of the quote   
    $('div[id^="templatePhase"]').each(function(){

        var $phaseHours = 0; 
        var phase_contingency_hours = 0; 
        var phase_hours_total = 0; 

        $(this).find(".hours").each(function (){
            var hours = Number(this.value); 
            console.log(hours);
            phase_hours_total += hours;

        });

      
        var phase_cost_total = phase_hours_total * 165; // this should be settable 
        phase_contingency_hours += Math.round((phase_hours_total * 0.2) * 100) / 100 ; // this should be settable 
        
        job_hours_low += phase_hours_total;
        job_hours_high += phase_hours_total + phase_contingency_hours;

        var phase_hours_total_high = phase_hours_total + phase_contingency_hours; 
        var phase_cost_total_high = phase_hours_total_high * 165; // this should be settable


        job_low_estimate = job_hours_low * 165; 
        job_high_estimate = Math.round((job_hours_high * 165) * 100) / 100 ;


        // now add the contingency hours to the phase totals 
        $(this).find(".phase_hours_total").val(phase_hours_total);
        $(this).find(".phase_cost_total").val("$"+phase_cost_total);
        $(this).find(".contingency_hours").val(phase_contingency_hours);



        console.log("contingency_hours = "+ contingency_hours); 
        

    });  

    $("#job_hours_total").text(job_hours_low);
    $("#job_hours_total_high").text(job_hours_high);
    $("#job_cost_total").text("$"+(job_low_estimate).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
    $("#job_cost_total_high").text("$"+(job_high_estimate).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
}



$("#btnShowDetails").click(function() {
  
    console.log("show details form");
    $("#details_phase1").toggle();

 

});

$("#btnAddPhase").click(function(){
   console.log("click");

   addPhase(); 
});


function addPhase(data)
{
    console.log("Adding Phase");
    phase_count++; 
    $div = $('div[id^="templatePhase"]:last');
    var num = parseInt( $div.prop("id").match(/\d+/g), 10 ) +1;
    if(isNaN(num)) num=2;
    var template  = $("#templatePhase").clone().prop('id', 'templatePhase'+num );
    template.prepend("<div>Phase "+num+"</div>").insertBefore("#add_phase_button_container");

    if(data !== undefined )
    {
        // if there's data use it to populate the form. 
        populateTemplateForm(template, data);
    }
   

    template.find('.hours').change(function(){ recalculateFees(); });



}


// only run this shit if this is an edit to the existing records

@if($edit)

var quote = {!! json_encode($quote) !!};

// now we loop through the object generating a copy of each phase form and then populating it with data 

// first let's set the pipedrive deal and lock it down so the user can't change this
$("#pipedrive_deal").val(quote.pipedrive_id);
$("#pipedrive_deal").attr('disabled', 'disabled');

$.each(quote.phases, function(k,v){

    // if we're in the first loop just populate the form that's here: 
    if(phase_count > 1)
    {
        addPhase(v);   
    }else{
         // if there's data use it to populate the form. 
        populateTemplateForm($('div[id^="templatePhase"]:first'), v);
    }
   
   phase_count++;

});




recalculateFees();

function populateTemplateForm($div, v)
{
    console.log("Populating phase ");
    console.log(v);
    $div.find('.phase_name').val(v.phase_name);
    $div.find('.phase_start_date').val(v.start_date);
    $div.find('.phase_end_date').val(v.end_date);
    
    // now we get into the lines 
    $.each(v.lines, function(key, value){
        
        var field = value.dept_name; 

        if(value.dept_name == 'Digital Marketing')
        {
            field = 'seo';
        } 

        $div.find('.hours_'+field.toLowerCase()).val(value.hours);
        $div.find('.description_'+field.toLowerCase()).val(value.note);
    });

}
  

@endif


</script>

@endsection