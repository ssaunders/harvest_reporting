@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            
                <h1>Hours for : {{ ucwords($staff) }}</h1>
                <h2>{{ $start }} - {{ $end }}</h2>
                <div class="panel panel-default">
                <div class="panel-body nopadding">
                    <div class="table-responsive table-hover">
                    <table class="table table-hover table-bordered" id="stickyHeader">
                      <thead >
                        <tr>  
                            <th class="col-md-3">Project Name</th>
                            <th class="col-md-3">Hours</th>
                            <th class="col-md-3">Project Rate</th>
                            <th class="col-md-3">Revenue</th>
                           
                        </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <td>Totals</td>
                                <td></td>
                                <td></td>
                                <td>{{ money_format('%1n', $sum) }}</td>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($project_list as $project_id=>$project)
                            <tr>
                                <td><a href="{{ route('projectdetails', ['project_id'=>$project_id]) }}">{{ $project->project_name }}</a> </td>
                                <td>{{ $project->hours }} </td>
                                <td>{{ $project->rate }}</td>
                                <td>{{ money_format('%1n',$project->revenue) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


            </div>
                </div>    
            </div>
        </div>
    </div>
</div>
@endsection
