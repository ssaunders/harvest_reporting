@extends('layout')

@section('content')

<h1>Revenue Hours for the Week of {{ $date->format('F d Y') }} to {{ $end_of_week }} </h1>
<h2>(based on timesheets and project rates)</h2>
<div class="ink-dropdown bottom-space" data-target="#date-dropdown">
    <button class="ink-button">Pick a Week</button>
    <ul id="date-dropdown" class="dropdown-menu">
    	@foreach($weeks as $week_num=>$display_date)
			<li><a href="{{ route('revenue', $week_num) }}">{{ $display_date }}</a></li>	
    	@endforeach
    </ul>
</div>


<div class="column-group">
    <div class="all-50">
    	<table class="ink-table">
		  <thead>
		    <tr>

		      <th class="align-left">Dept</th>
		      <th class="align-left">Revenue</th>
		      <th class="align-left">Target</th>
		      <th class="align-left">Percent</th>
		  </tr>
		 </thead>

@foreach($revenue_by_dept as $key => $value) 
        
          
            <tr>
	            <td> {{ ucwords(str_replace("_", " ", $key)) }} </td>
	            <td> {{ money_format('%1n', $value) }}</td>
	            <td> {{ money_format('%1n',($dept_salaries[$key]/47)) }} </td>
	            <td @if($percent_to_target_by_dept[$key] >= 100)  class='ink-label green' @else class='ink-label red' @endif> {{ $percent_to_target_by_dept[$key] }}%</td>
            </tr>
@endforeach        
     		<tr>
     			<td>Total Revenue</td>
     			<td> {{ money_format('%1n', $total_revenue_for_week) }} </td>
     			<td> {{ money_format('%1n', $total_salary_cost_for_week) }} </td>
     			<td @if($percent_to_target >= 100)  class='ink-label green' @else class='ink-label red' @endif > {{ $percent_to_target}}% </td>
     		</tr>
       </table>
	</div>
</div>
<h3>Week Details by Employee</h3>
<div class="column-group horizontal-gutters">
	@foreach($hours_by_user as $key => $value)
		<div class="all-33">
			<h4>{{ $key }}</h4>
			<table class="ink-table">
				<thead>
					<th>Project</th>
					<th>Billable</th>
					<th>Non-billable</th>
				</thead>
				
				@foreach($value as $project_name=>$hours)
				 
            	  

					<tr>
						<td> {{ $project_name }} </td>
						<td> {{ $hours->billable }} </td>
						<td> {{ $hours->non_billable }} </td>
					</tr>
	               
				@endforeach

			</table>
		</div>
	@endforeach
</div>
@endsection