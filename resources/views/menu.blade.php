<nav class="ink-navigation">
    <ul class="menu horizontal black">
        <li><a href="{{ route('invoices')}}">Invoices</a></li>
        <li><a href="{{ route('revenue')}}">Weekly Revenue</a></li>
        <li><a href="{{ route('projects')}}">Projects</a></li>
    </ul>
</nav>