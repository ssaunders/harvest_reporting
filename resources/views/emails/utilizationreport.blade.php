@component('mail::message')
# Utilization Report

Here's your utilization report for the {{ $dept }} team.
## {{ $human_date }}

@component('mail::panel')
@component('mail::table')

|Staff|Billable Hours|Revenue Generated|
| :----------- | :-----------: | -----------: |
@foreach($data->hours_by_dept_by_employee[$dept] as $staff_name=>$staff)
|{{$staff_name}}|{{$staff}}|{{ money_format('%1n', $data->revenue_by_employee[$dept][$staff_name]) }}|
@endforeach
@endcomponent

Your team generated a total of {{ money_format('%1n', $data->revenue_by_dept[$dept]) }}.  
Your team's cost last week was {{ money_format('%1n',$costs[$dept]->dept_cost) }}.  
This is a net @if($data->revenue_by_dept[$dept] > $costs[$dept]->dept_cost) gain @else loss @endif of {{ money_format('%1n',$data->revenue_by_dept[$dept] - $costs[$dept]->dept_cost)  }}.


@endcomponent


Thanks,<br>
{{ config('app.name') }}
@endcomponent
